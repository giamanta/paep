# Akka
Framework for programming actor-based programs in Scala/Java.

*NB:*
```
Based on Erlang (language inside 90% of routers).
Really robust systems
```

When we create an actor, we create him from another one that will be his
supervisor, that handle "child" behavior.

## ACTORS BEST PRACTICE
  * must **never** _BLOCK_
  * Actors should be like nice co-workers:
    1) do their job efficiently without bothering everyone else needlessly
    and avoid hogging resources.
    2) translated to programming this means to process events and
    generate responses (or more requests) in an event-driven manner
    3) this means that actors should never block (i.e. passively wait while
    occupying a Thread) on some external entity—which might be a lock,
    a network socket, etc.—unless it is unavoidable
  * Do not pass mutable objects between actors.
    1 in order to ensure that, prefer immutable messages.
    2 if the encapsulation of actors is broken by exposing their mutable
    state to the outside, you are back in normal Java concurrency land
    with all the drawbacks.

