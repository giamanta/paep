package pap.ass08.goAl;

public class ComputeMsg {

	private int flux;

	public ComputeMsg(int flux){
		this.flux = flux;
	}

	public int getFlux(){
		return this.flux;
	}
}
