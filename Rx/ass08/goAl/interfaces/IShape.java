package pap.ass08.goAl.interfaces;

import pap.ass08.goAl.*;

/**
 * Interfaccia che rappresenta una figura in una viewport grafica (0,0)-(w,h)
 * 
 * @author aricci & gmantani
 *
 */
public interface IShape {
	public void move(V2d v);

	public boolean isInside(BBox box);

	public boolean contains(P2d p);

	public BBox getBBox();
	
	public int getNPixels();
	
	public Object linesLen();
	
	public int lines();
}
