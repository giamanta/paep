package pap.ass08.goAl.interfaces;

import pap.ass08.goAl.GameOfLife;

public interface IGameOfLifeListener {

	public void update(GameOfLife game);
}
