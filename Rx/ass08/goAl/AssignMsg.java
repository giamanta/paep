package pap.ass08.goAl;

public class AssignMsg {

	private Controller.Msg msg;
	private int nactors;

	public AssignMsg(Controller.Msg what, int nactors) {
		this.msg = what;
		this.nactors = nactors;
	}

	public Controller.Msg getWhat() {
		return this.msg;
	}

	public int getNactors() {
		return this.nactors;
	}
}
