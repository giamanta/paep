package pap.ass08.goAl;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class Controller extends UntypedActor {

	public static enum Msg {
		START, INSTART, STOP, GEN, GEND, CEND;
	}

	private final int FPS = 60;

	private GameOfLife game;
	private GameOfLifeView view;
	private ActorSystem system;
	private int nsons, npoints, dx;
	private ArrayList<ActorRef> generators, computers;
	private ArrayList<Integer> resultGen, resultCom;
	private long t0Gen, t0Com;
	private boolean stopFlag;

	public Controller(GameOfLife game, GameOfLifeView view, int nsons) {
		this.game = game;
		this.view = view;
		this.nsons = nsons;
		this.npoints = game.getCells().length;
		this.dx = npoints / nsons;
		this.stopFlag = false;
		system = ActorSystem.create("GameOfActorLife");
	}

	public void preStart() {
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof Msg) {
			switch (((Msg) msg)) {
			case START:
				this.stopFlag = false;
			case INSTART:
				log("start new generation");

				if (computers == null) { // Create the list
					computers = new ArrayList<ActorRef>();
					resultCom = new ArrayList<Integer>();
				} else { // or clear it in order to handle a new generation
					computers.clear();
					resultCom.clear();
				}
				t0Com = System.currentTimeMillis();
				for (int i = 1, x0 = 0; i <= nsons; i++, x0 += dx) {
					computers.add(system.actorOf(Props.create(
							ComputeStripeTask.class, x0, (i == nsons) ? npoints
									: x0 + dx, game), "Computer" + i));
					log("created ComputeStripeTaskActor " + x0 + " "
							+ (x0 + dx));
					computers.get(i - 1).tell(Msg.START, getSelf());
				}
				break;

			case STOP:
				log("stop");
				this.stopFlag = true;
				// say to all actors to stop!
				if (generators != null || !generators.isEmpty())
					generators.stream().forEach(
							g -> g.tell(PoisonPill.getInstance(),
									ActorRef.noSender()));
				if (computers != null || !computers.isEmpty())
					computers.stream().forEach(
							g -> g.tell(PoisonPill.getInstance(),
									ActorRef.noSender()));
				break;

			case GEN:
				log("generate board");
				this.game = new GameOfLife(game.getPerimeter());
				// We must generate the board, we could use as much actor as we
				// want (at most one per cell)
				if (generators == null) {
					generators = new ArrayList<ActorRef>();
					resultGen = new ArrayList<Integer>();
				} else {
					generators.clear();
					resultGen.clear();
				}
				t0Gen = System.currentTimeMillis();
				for (int i = 1, x0 = 0; i <= nsons; i++, x0 += dx) {
					generators.add(system.actorOf(Props.create(
							GenerateStripeTask.class, x0,
							(i == nsons) ? npoints : x0 + dx, game),
							"Generator" + i));
					generators.get(i - 1).tell(Msg.GEN, getSelf());
					log("created GenerateStripeTaskActor " + x0 + " "
							+ (x0 + dx));
				}
				break;

			case GEND:
				game.updateState(resultGen.stream().reduce((v1, v2) -> v1 + v2)
						.get());
				view.setUpdated(game);
				long t1 = System.currentTimeMillis();
				view.changeState("Creation done, elapsed: " + (t1 - t0Gen)
						+ "ms");
				break;

			case CEND:
				game.updateState(resultCom.stream().reduce((v1, v2) -> v1 + v2)
						.get());
				view.setUpdated(game);
				long t1c = System.currentTimeMillis();
				view.changeState("Generation done, elapsed: " + (t1c - t0Com)
						+ "ms");
				break;
			default:
				unhandled(msg);
				break;
			}
		}
		if (msg instanceof GenerateMsg) {
			resultGen.add(((GenerateMsg) msg).getFlux());
			if (resultGen.size() == generators.size())
				getSelf().tell(Msg.GEND, ActorRef.noSender());
		}
		if (msg instanceof ComputeMsg) {
			resultCom.add(((ComputeMsg) msg).getFlux());
			if (resultCom.size() == computers.size()) {
				// Now I should wait framerate ms before compute the next
				// generation
				if (stopFlag == false)
					system.scheduler().scheduleOnce(
							Duration.create(FPS, TimeUnit.MILLISECONDS),
							getSelf(), Msg.INSTART, getContext().dispatcher(),
							null);
				getSelf().tell(Msg.CEND, ActorRef.noSender());
			}
		} else
			unhandled(msg);
	}

	private void log(String msg) {
		System.out.println("[Controller-" + getSelf() + "]\n\t->" + msg);
	}
}
