package pap.ass08.goAl;

import pap.ass08.goAl.Controller.Msg;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.japi.Creator;

public class GenerateStripeTask extends UntypedActor {

	private GameOfLife result;
	private int from;
	private int to;

	public GenerateStripeTask(int from, int to, GameOfLife result) {
		this.result = result;
		this.from = from;
		this.to = to;
	}

	/**
	 * Create Props for an actor of this type. It is a good idea to provide
	 * static factory methods on the UntypedActor which help keeping the
	 * creation of suitable Props as close to the actor definition as possible.
	 * This also allows usage of the Creator-based methods which statically
	 * verify that the used constructor actually exists instead relying only on
	 * a runtime check. Source : http://doc.akka.io/docs/akka
	 * /2.4-M1/java/untyped-actors.html#untyped-actors-java
	 *
	 * @return a Props for creating this actor, which can then be further
	 *         configured (e.g. calling `.withDispatcher()` on it)
	 */
	public static Props props(final int from, final int to,
			final GameOfLife game) {
		return Props.create(new Creator<GenerateStripeTask>() {
			private static final long serialVersionUID = 1L;

			@Override
			public GenerateStripeTask create() throws Exception {
				return new GenerateStripeTask(from, to, game);
			}
		});
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof Controller.Msg) {
			if ((Controller.Msg) msg == Msg.GEN) {
				int flux = 0;
				log("start generating");
				int nSteps = 4;
				int dx = (to - from) / nSteps;
				for (int i = 1; i <= nSteps; i++, from += dx)
					flux += result.generateSlice(from, (i == nSteps) ? to
							: from + dx);
				log("generation completed");
				getSender().tell(new GenerateMsg(flux), getSelf());
				getSelf().tell(PoisonPill.getInstance(), getSelf()); // Suicide
			}
		}
	}

	private void log(String msg) {
		synchronized (System.out) {
			System.out.println("[GenerateStripeTaskActor-" + getSelf()
					+ "]\n\t->" + msg);
		}
	}
}
