package pap.ass08.goAl;

import akka.actor.ActorSystem;
import akka.actor.Props;

public class Main {
	public static void main(String[] args) {
		int w = 999;
		int h = 555;

		GameOfLife model = new GameOfLife(
				new Rect(new P2d(0, 0), new P2d(w, h)));
		GameOfLifeView view = new GameOfLifeView(w, h);
		view.addListener(ActorSystem.create().actorOf(
				Props.create(Controller.class, model, view, Runtime
						.getRuntime().availableProcessors() + 1), "Controller"));
		view.setVisible(true);
	}
}
