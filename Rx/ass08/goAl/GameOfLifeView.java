package pap.ass08.goAl;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import akka.actor.ActorRef;

public class GameOfLifeView extends JFrame implements ActionListener {

	/**
	 * Default serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private JButton startButton;
	private JButton newButton;
	private JButton stopButton;
	private JTextField state;
	private JLabel deca, lives;
	private GameOfLifePanel setPanel;
	private ArrayList<ActorRef> listeners;

	public GameOfLifeView(int w, int h) {
		super("GameOfLife Viewer");
		setSize(w + 150, h + 120);
		listeners = new ArrayList<ActorRef>();

		newButton = new JButton("new");
		startButton = new JButton("start");
		startButton.setEnabled(false);
		stopButton = new JButton("stop");
		stopButton.setEnabled(false);
		JPanel controlPanel = new JPanel();
		controlPanel.add(newButton);
		controlPanel.add(startButton);
		controlPanel.add(stopButton);

		setPanel = new GameOfLifePanel(w, h);
		setPanel.setSize(w, h);

		JPanel infoPanel = new JPanel();
		state = new JTextField(20);
		state.setText("Infos state");
		state.setEditable(false);
		deca = new JLabel("Decade [0]");
		lives = new JLabel("Live Cells [0]");
		infoPanel.add(deca);
		infoPanel.add(lives);
		infoPanel.add(state);
		JPanel cp = new JPanel();
		LayoutManager layout = new BorderLayout();
		cp.setLayout(layout);
		cp.add(BorderLayout.NORTH, controlPanel);
		cp.add(BorderLayout.CENTER, setPanel);
		cp.add(BorderLayout.SOUTH, infoPanel);
		setContentPane(cp);

		newButton.addActionListener(this);
		startButton.addActionListener(this);
		stopButton.addActionListener(this);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void setUpdated(final GameOfLife game) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				setPanel.updateImage(game.getCells());
				deca.setText("Decade [" + game.getDecade() + "]");
				lives.setText("Live Cells [" + game.getLives() + "]");
			}
		});
	}

	public void changeState(final String s) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				state.setText(s);
			}
		});
	}

	public void addListener(ActorRef l) {
		listeners.add(l);
	}

	public void actionPerformed(ActionEvent ev) {
		String cmd = ev.getActionCommand();
		if (cmd.equals("start")) {
			notifyStarted();
			newButton.setEnabled(false);
		} else if (cmd.equals("stop")) {
			notifyStopped();
			newButton.setEnabled(true);
		} else if(cmd.equals("new")){
			notifyNew();
			startButton.setEnabled(true);
			stopButton.setEnabled(true);
		}
	}

	private void notifyStarted() {
		for (ActorRef l : listeners)
			l.tell(Controller.Msg.START, ActorRef.noSender()); 
	}

	private void notifyStopped() {
		for (ActorRef l : listeners)
			l.tell(Controller.Msg.STOP, ActorRef.noSender()); 
	}

	private void notifyNew() {
		for (ActorRef l : listeners)
			l.tell(Controller.Msg.GEN, ActorRef.noSender()); 
	}
}
