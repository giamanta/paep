# Assignment 8 (20150528)

#### Esercizio su Reactive Programming in RxJava - "Temperature Monitoring"
Testo:
```
Nel package pap.ass08 è data una classe TempSensor che rappresenta un sensore
di temperatura.

A partire dal sensore, definire un observable TempStream  che rappresenti uno
stream asincrono di valori di temperatura, generando i valori (letture) ad
intervalli regolari, con una certa frequenza (specificabile via costruttore).

Si suppone di voler costruire un’applicazione di monitoraggio della temperatura
di un certo ambiente  in cui si utilizzano tre sensori.

Dati tre TempStream corrispondenti ai tre sensori, l’applicazione deve creare
un ulteriore observable AverageTempStream che contiene il valore medio dei
valori generati dai singoli stream, opportunamente filtrati in modo da evitare
di considerare degli spike, ovvero letture errate. Una lettura è ritenuta
errata se si discosta rispetto alla precedente per più di un valore
MaxVariation.

In una finestra grafica devono essere visualizzati man mano i valori prodotti
dallo stream AverageTempStream, nonché il valore massimo e minimo prodotti fino
a quel momento.

La finestra deve avere anche due pulsanti, start e stop, con cui far partire e
fermare il monitoraggio.

Qualora il valore di temperatura medio si maggiore di una certa soglia -
specificabile e modificabile  dalla GUI - per più di un certo tempo in
millisecondi - specificabile e modificabile via GUI - allora deve essere
visualizzato (sempre nella finestra) un messaggio di allarme.
```
* `tempMon/Timon.java` is the init (main) class.
* `tempMon/Controller.java` is the central actor where all observables are
  created and that handle start and stop.
* `tempMon/InputListener.java` interface.
* `tempMon/Pair.java` a class that help me filter the spikes, in
  `tempMon/TempStream.java` I save the current value in the left and the
  temperature difference in the right.

  **NB**: Here we could have a problem in handle a spike in the first value, but
  is not so crucial fix it, I prefer a system that work from the second value
  from a system that skip good output at startup in order to recognize and
  discard values. Reboot and it works fine from the first one.
* `tempMon/StopFlag.java` used to stop core thread `TempStream`.
* `tempMon/TempSensor.java` provided.
* `tempMon/TempStream.java` thread that generate continuously new temperatures.
* `tempMon/TestSensor.java` provided.
* `tempMon/View.java` a simple GUI.

#### Esercizio su Attori - "Game of (Actor) Life"
```
Implementare il gioco “Game of Life”  discusso in un compito precedente
utilizzando il modello degli attori e la tecnologia actor-based Akka.
```
I have used an MVC pattern like the one used in the _assignment06_:

* [_MODEL_] `goAl/GameOfLife.java`
* [_VIEW_] `goAl/GameOfLifeView.java`
* [_CONTROLLER_] `goAl/Controller.java`

The core is the same. Now `goAl/Controller.java` is an actor that has others
actors like executors and a scheduler that every new generation if there isn't a
stop action from the UI send a self-message in order to compute new values.

## Resources
* [ReactiveX wiki](https://github.com/ReactiveX/RxJava/wiki)
* [rx tuts](http://reactivex.io/tutorials.html)
* [rxmarbles](http://rxmarbles.com/)
* [observable,operators,subject,scheduler](http://reactivex.io/documentation/operators.html)
* [Your mouse is a DB Erik Meijer](http://queue.acm.org/detail.cfm?id=2169076)
