package pap.ass08.tempMon;

import rx.Subscriber;

public class TempStream extends Thread {
	private TempSensor sensor;
	private Subscriber<? super Pair<Integer, Integer>> subscriber;
	private int hz;
	private StopFlag monFlag;
	private Integer prevTemp, tempDiff;

	public TempStream(Subscriber<? super Pair<Integer, Integer>> subscriber, TempSensor sensor, int hz){
		this.monFlag = new StopFlag();
		init(subscriber, sensor, hz);
	}

	public TempStream(Subscriber<? super Pair<Integer, Integer>> subscriber, TempSensor sensor, int hz, StopFlag monFlag){
		this.monFlag = monFlag;
		init(subscriber, sensor, hz);
	}

	private void init(Subscriber<? super Pair<Integer, Integer>> subscriber, TempSensor sensor, int hz) {
		this.subscriber = subscriber;
		this.sensor = sensor;
		this.hz = hz;
		this.prevTemp = Integer.MAX_VALUE;
	}

	public int getTempDiff(){
		return prevTemp;
	}

	@Override
	public void run() {
		Integer cv;
		while (!monFlag.isDone()) {
			try {
				cv = (int) sensor.getCurrentValue();
				if(this.prevTemp == Integer.MAX_VALUE)
					this.tempDiff = 0;
				else
					this.tempDiff = Math.abs(this.prevTemp - cv);
				subscriber.onNext(new Pair<Integer, Integer>(cv, tempDiff));
				this.prevTemp = cv;
				Thread.sleep(hz);
			} catch (Exception ex) {
				subscriber.onError(ex);
			}
		}
		subscriber.onCompleted();
	}
}
