package pap.ass08.tempMon;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import rx.Observable;

public class View extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton startButton;
	private JButton stopButton;
	private JList<Integer> ltemps;
	private DefaultListModel<Integer> lmodel;
	private JLabel lwarning;
	private JTextField tfvalMax, tfvalMin, tfsoglia, tfmsec;
	private Integer vmin, vmax;
	private ArrayList<InputListener> listeners;
	private int soglia, msec;
	private long t0, t1;

	public View(int w, int h) {
		super("Temperature Monitoring");
		listeners = new ArrayList<InputListener>();
		setSize(w, h);

		startButton = new JButton("start");
		stopButton = new JButton("stop");
		stopButton.setEnabled(false);
		JPanel controlPanel = new JPanel();
		controlPanel.add(startButton);
		controlPanel.add(stopButton);

		JPanel listPanel = new JPanel();
		listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.PAGE_AXIS));
		lmodel = new DefaultListModel<Integer>();
		ltemps = new JList<Integer>(lmodel);
		listPanel.add(ltemps);
		ScrollPane sp = new ScrollPane();
		sp.add(listPanel, BorderLayout.CENTER);

		JPanel infoPanel = new JPanel();
		lwarning = new JLabel("WARNING");
		lwarning.setFont(new Font("warning", ERROR, 25));
		lwarning.setForeground(Color.RED);
		lwarning.setVisible(false);
		tfvalMax = new JTextField(4);
		tfvalMax.setText("---");
		tfvalMax.setMaximumSize(new Dimension(250, 20));
		tfvalMax.setEditable(false);
		tfvalMin = new JTextField(4);
		tfvalMin.setText("---");
		tfvalMin.setMaximumSize(new Dimension(250, 20));
		tfvalMin.setEditable(false);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		infoPanel.add(lwarning);
		infoPanel.add(new JLabel("Val Max"));
		infoPanel.add(tfvalMax);
		infoPanel.add(new JLabel("Val Min"));
		infoPanel.add(tfvalMin);
		tfsoglia = new JTextField(4);
		tfsoglia.setMaximumSize(new Dimension(250, 20));
		tfsoglia.setText("0");
		tfmsec = new JTextField(4);
		tfmsec.setMaximumSize(new Dimension(250, 20));
		tfmsec.setText("0");
		infoPanel.add(new JLabel("Soglia"));
		infoPanel.add(tfsoglia);
		infoPanel.add(new JLabel("msec"));
		infoPanel.add(tfmsec);
		JPanel cp = new JPanel();
		LayoutManager layout = new BorderLayout();
		cp.setLayout(layout);
		cp.add(BorderLayout.NORTH, controlPanel);
		cp.add(BorderLayout.WEST, sp);
		cp.add(BorderLayout.CENTER, infoPanel);
		setContentPane(cp);

		startButton.addActionListener(this);
		stopButton.addActionListener(this);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void init(Observable<Integer> obs) {
		this.startButton.setEnabled(false);
		this.stopButton.setEnabled(true);
		vmin = Integer.MAX_VALUE;
		vmax = Integer.MIN_VALUE;
		this.tfvalMax.setText("---");
		this.tfvalMin.setText("---");
		this.lwarning.setVisible(false);
		this.lmodel.clear();
		this.tfsoglia.setEnabled(false);
		this.tfmsec.setEnabled(false);
		this.soglia = Integer.parseInt(tfsoglia.getText());
		this.msec = Integer.parseInt(tfmsec.getText());
		this.t0 = System.currentTimeMillis();
		obs.subscribe((Integer i) -> {
			if (i > vmax) {
				vmax = i;
				tfvalMax.setText("" + vmax);
			}
			if (i < vmin) {
				vmin = i;
				tfvalMin.setText("" + vmin);
			}

			t1 = System.currentTimeMillis();
			if (i >= this.soglia) {
				if (t1 - t0 >= msec)
					this.lwarning.setVisible(true);
			} else
				t0 = System.currentTimeMillis();
			this.lmodel.addElement(i);
		});
	}

	public void deinit(Observable<Integer> obs) {
		this.startButton.setEnabled(true);
		this.stopButton.setEnabled(false);
		this.tfsoglia.setEnabled(true);
		this.tfmsec.setEnabled(true);
	}

	public void actionPerformed(ActionEvent ev) {
		String cmd = ev.getActionCommand();
		if (cmd.equals("start")) {
			for (InputListener l : listeners) {
				l.started();
			}
		} else if (cmd.equals("stop")) {
			for (InputListener l : listeners) {
				l.stopped();
			}
		}
	}

	public void addListener(InputListener l) {
		listeners.add(l);
	}
}
