package pap.ass08.tempMon;

public class Timon {
	public static void main(String[] args) {

		int MaxVariation = 5;
		View view = new View(245, 300);
		Controller controller = new Controller(view, MaxVariation);
		view.addListener(controller);
		view.setVisible(true);
	}

}
