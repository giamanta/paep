package pap.ass08.tempMon;

public class StopFlag {

	private boolean done;
	
	public StopFlag(){
		done = true;
	}
	
	public synchronized void flip(){
		done = !done;
	}

	public synchronized boolean isDone(){
		return done;
	}
	
}