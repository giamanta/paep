package pap.ass08.tempMon;

import rx.Observable;
import rx.Subscriber;

public class Controller implements InputListener {

	private StopFlag monFlag;
	private TempSensor sens1, sens2, sens3;
	private Observable<Integer> s1, s2, s3, avg;
	private View view;
	private int maxVariation;

	public Controller(View view, int maxVariation) {
		this.view = view;
		this.maxVariation = maxVariation;
		monFlag = new StopFlag();
		sens1 = new TempSensor(50, 75, 0.01);
		sens2 = new TempSensor(30, 55, 0.01);
		sens3 = new TempSensor(-10, 5, 0.01);

		// sensors
		s1 = Observable
				.create((Subscriber<? super Pair<Integer, Integer>> subscriber) -> {
					new TempStream(subscriber, sens1, 225, monFlag).start();
				}).filter(t -> t.getRight() <= this.maxVariation)
				.map(t -> t.getLeft());

		s2 = Observable
				.create((Subscriber<? super Pair<Integer, Integer>> subscriber) -> {
					new TempStream(subscriber, sens2, 325, monFlag).start();
				}).filter(t -> t.getRight() <= this.maxVariation)
				.map(t -> t.getLeft());

		s3 = Observable
				.create((Subscriber<? super Pair<Integer, Integer>> subscriber) -> {
					new TempStream(subscriber, sens3, 425, monFlag).start();
				}).filter(t -> t.getRight() <= this.maxVariation)
				.map(t -> t.getLeft());

		// avg sensors
		avg = Observable.zip(s1, s2, s3, (v1, v2, v3) -> {
			return (v1 + v2 + v3) / 3;
		});
	}

	public void started() {
		this.monFlag.flip();
		view.init(this.avg);
	}

	public void stopped() {
		this.monFlag.flip();
		view.deinit(this.avg);
	}
}
