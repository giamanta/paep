Rx is only a little piece of the paradigms cake, a cake without a defined
perimeter.

Agents are reactive actor proactive!!! Rx is not proactive.

Observable
* synchronous, sequential
* asynchronous,

Observable and subscriber could be executed from different thread!

`just` and `from` make a stream from a structure already defined
`create` more generic, you could build at runtime the structure

## Assignment 08
### 1
First of all do the merge of all sensors and after
that,[avarage](http://rxmarbles.com/#average). Values in sensors could be wrong,
we call them spike, to detect them see if there is a too elevate difference
between a value and the next one. These values should be filtered and not used
in the average. If a values is bigger than a value -> spike, if there are lots
of spike in a range of time T, stop monitoring!

### 2
The new problem could be, how can we use `akka` with `swing`? See the prof post
in the forum. Event dispatcher thread is an actor! And you could use the same
API, `invoke` and `invokeLater`.

Try to solve without misc different concurrents models, actor should not share
data (same object), actor should not use executors, barriers etc.

##Resources
* [rxwiki](http://rxwiki.wikidot.com/)
* [interactive marbles](http://rxmarbles.com/#average)
* [marbles ascii](https://github.com/richardszalay/raix/wiki/Marble-Diagrams)
