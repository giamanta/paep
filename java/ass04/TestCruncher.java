package pap.ass04;

import java.util.stream.IntStream;

public class TestCruncher {
	public static void main(String[] args) {
		Long maxLimit = Long.parseLong(args[0]);
		Secret s = new Secret(maxLimit);

		int howMany = Runtime.getRuntime().availableProcessors();
		long chunk = maxLimit / howMany;
		long carry = maxLimit % howMany;

		long t0 = System.currentTimeMillis();

		IntStream.range(0, howMany).forEach(
				i -> {
					WorkerBrute wb = new WorkerBrute("bruter" + i, s,
							chunk * i, (i == howMany ? (chunk * i) + chunk + carry
									: chunk * i + chunk));
					wb.start();
				});

		long t1 = System.currentTimeMillis();
		System.out.println("Time elapsed: " + (t1 - t0));
	}
}