package pap.ass04;

public abstract class Worker extends Thread {
	
	Secret so;
	long from, size;
	
	public Worker(String name, Secret so, long from, long size){
		super(name);
		this.so = so;
		this.from = from;
		this.size = size;
	}

	protected void print(String msg){
		synchronized (System.out){
			System.out.print(msg);
		}
	}

	protected void println(String msg){
		synchronized (System.out){
			System.out.println(msg);
		}
	}

}