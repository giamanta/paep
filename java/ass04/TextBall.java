package pap.ass04;

import java.util.stream.IntStream;

public class TextBall {

	public static void main(String[] args) {

		TextContext ctx = new TextContext(TextLibFactory.getInstance());
		IntStream.rangeClosed(0, Integer.parseInt(args[0])).forEach(
				i -> ctx.createNewBall());
		
		BallTextViewer btv = new BallTextViewer(ctx);
		btv.start();

	}

}
