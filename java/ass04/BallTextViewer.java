package pap.ass04;

import java.util.Arrays;

import pap.demo.common.*;

public class BallTextViewer extends Thread {

	private String st = "*";
	private int color = 1;
	private boolean stop;
	private TextContext context;
	private static final int FRAMES_PER_SEC = 25;

	public BallTextViewer(TextContext context) {
		stop = false;
		this.context = context;
	}

	public void run() {
		while (!stop) {
			long t0 = System.currentTimeMillis();
			updatePosition();
			long t1 = System.currentTimeMillis();
			// log("update pos");
			long dt = (1000 / FRAMES_PER_SEC) - (t1 - t0);
			if (dt > 0) {
				try {
					Thread.sleep(dt);
				} catch (Exception ex) {
				}
			}
		}
	}

	private void updatePosition() {
		context.getFactory().cls();
		P2d[] positions = context.getPositions();
        synchronized (this){
            if (positions!=null){
            	int dx = 120/2 - 10; // This fit well with my terminal
            	int dy = 50/2 - 10;	 // I haven't find a reliable way cross-platform to get x & y term
                Arrays.stream(positions).forEach( p -> {
                	int x0 = (int)(dx+p.x*dx);
	                int y0 = (int)(dy-p.y*dy);
	                context.getFactory().writeAt(x0, y0, st, color);
	            });
            }
        }
	}

	@SuppressWarnings("unused")
	private void log(String msg) {
		System.out.println("[VISUALISER] " + msg);
	}

}
