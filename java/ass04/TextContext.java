package pap.ass04;

public class TextContext extends Context {

	TextLib factory;

	public TextContext(TextLib factory) {
		super();
		this.factory = factory;
	}

	public TextLib getFactory() {
		return this.factory;
	}

}
