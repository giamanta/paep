package pap.ass04;

import java.util.stream.LongStream;

public class WorkerBrute extends Worker {

	public WorkerBrute(String name, Secret so, long from, long size) {
		super(name, so, from, size);
	}

	public void run() {
		LongStream.range(from, size).forEach(
				i -> {
					if (so.guess(i)) {
						println("[!] SECRET CRACKED: " + i
								+ " by " + getName() + "... please keep it secret!");
						System.exit(0);
					}
				});
	}
}
