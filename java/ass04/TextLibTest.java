package pap.ass04;

public class TextLibTest {

	public static void main(String[] args) {
		
		TextLib lib = TextLibFactory.getInstance();
		lib.cls();
		lib.writeAt(10, 5, "*");
		lib.writeAt(3, 2, "*", 1);
	}

}
