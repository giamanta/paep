package pap.ass07.ring;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class DumbPlayerActor extends UntypedActor {

	private Integer guess;
	private ActorRef neighbor;
	private ActorRef oracle;
	private boolean firstOneFlag;
	private int turn;

	public void preStart() {
		log("ready to play");
		guess = 0;
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof OracleActor.Msg) {
			switch ((OracleActor.Msg) msg) {
			case LESS:
				guess--;
				neighbor.tell(OracleActor.Msg.TRY, getSelf());
				break;

			case MORE:
				guess++;
				neighbor.tell(OracleActor.Msg.TRY, getSelf());
				break;

			case GUESSED:
				log("won!");
				neighbor.tell(OracleActor.Msg.LOSER, getSelf());
				getContext().stop(getSelf());
				break;

			case LOSER:
				log("sob!");
				neighbor.tell(OracleActor.Msg.LOSER, getSelf());
				getContext().stop(getSelf());
				break;

			case TRY:
				if(firstOneFlag)
					log("Turn: " + turn++);
				oracle.tell(guess, getSelf());
				break;
			}
		} else if (msg instanceof AssignMsg)
			oracle = ((AssignMsg) msg).getTarget();
		else if (msg instanceof NeighborMsg){
			neighbor = ((NeighborMsg) msg).getNeighbor();
			firstOneFlag = ((NeighborMsg) msg).getFirstFlag();
		}
		else
			unhandled(msg);
	}

	private void log(String msg) {
		System.out.println("[DumbPlayerActor-" + getSelf() + "] " + msg);
	}
}
