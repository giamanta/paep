package pap.ass07.ring;

import akka.actor.ActorRef;

public class NeighborMsg {
	private ActorRef neighbor;
	private boolean firstOne;

	public NeighborMsg(ActorRef neighbor){
		this.neighbor = neighbor;
		this.firstOne = false;
	}

	public ActorRef getNeighbor(){
		return neighbor;
	}
	
	public void setFirstFlag(){
		this.firstOne = true;
	}
	
	public boolean getFirstFlag(){
		return this.firstOne;
	}
}
