package pap.ass07.ring;

import akka.actor.ActorRef;

public class AssignMsg {

	private ActorRef target;

	public AssignMsg(ActorRef target){
		this.target = target;
	}

	public ActorRef getTarget(){
		return target;
	}
}
