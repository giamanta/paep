package pap.ass07.ring;

import akka.actor.ActorSystem;
import akka.actor.Props;

public class Main {
	public static void main(String[] args) {
		ActorSystem system = ActorSystem.create("GuessTheNumberRing");
		system.actorOf(Props.create(BootActor.class, Integer.parseInt(args[0])));
	}
}
