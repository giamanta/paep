package pap.ass07.ring;

import java.util.*;
import java.util.stream.IntStream;

import akka.actor.*;
import akka.japi.Creator;

public class BootActor extends UntypedActor {

	private int nPlayers;

	public BootActor(int players) {
		this.nPlayers = players;
	}

	/**
	 * Create Props for an actor of this type. It is a good idea to provide
	 * static factory methods on the UntypedActor which help keeping the
	 * creation of suitable Props as close to the actor definition as possible.
	 * This also allows usage of the Creator-based methods which statically
	 * verify that the used constructor actually exists instead relying only on
	 * a runtime check. Source : http://doc.akka.io/docs/akka
	 * /2.4-M1/java/untyped-actors.html#untyped-actors-java
	 *
	 * @param players
	 *            collection of all players
	 * @return a Props for creating this actor, which can then be further
	 *         configured (e.g. calling `.withDispatcher()` on it)
	 */
	public static Props props(final int players) {
		return Props.create(new Creator<BootActor>() {
			private static final long serialVersionUID = 1L;

			@Override
			public BootActor create() throws Exception {
				return new BootActor(players);
			}
		});
	}

	public void preStart() {
		try{
		log("running");
		Random r = new Random(System.nanoTime());
		ArrayList<ActorRef> players = new ArrayList<ActorRef>();
		ActorRef oracle = getContext().actorOf(Props.create(OracleActor.class),
				"Oracle");
		IntStream.range(0, nPlayers).forEach(
				i -> players.add(getContext().actorOf(
						Props.create(r.nextBoolean() ? SmartPlayerActor.class
								: DumbPlayerActor.class), "Player" + i)));
		log("players created.");
		NeighborMsg nmsg = new NeighborMsg(players.get(1));
		nmsg.setFirstFlag();
		players.get(0).tell(new AssignMsg(oracle), getSelf());
		players.get(0).tell(nmsg, getSelf());
		IntStream.range(1, nPlayers).forEach(
				i -> {
					players.get(i).tell(new AssignMsg(oracle), getSelf());
					players.get(i).tell(
							new NeighborMsg(players.get((i + 1) % nPlayers)),
							getSelf());
				});
		log("players informed.");
		players.get(0).tell(OracleActor.Msg.TRY, getSelf());
		log("first play should start!");
		}catch(Exception e){
			log("Error, have you entered more than 1 player? Exception is:" + e);
		}
	}

	@Override
	public void onReceive(Object msg) {
	}

	private void log(String msg) {
		System.out.println("[BootActor] " + msg);
	}

}
