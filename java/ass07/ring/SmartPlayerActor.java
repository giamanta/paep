package pap.ass07.ring;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class SmartPlayerActor extends UntypedActor {

	private Integer guess, edx, esx;
	private ActorRef neighbor;
	private ActorRef oracle;
	private boolean firstOneFlag;
	private int turn;
	private int tmp;

	public void preStart() {
		log("ready to play");
		turn = 0;
		guess = 0;
		edx = Integer.MAX_VALUE;
		esx = Integer.MIN_VALUE;
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof OracleActor.Msg) {
			switch ((OracleActor.Msg) msg) {
			case LESS:
				edx = guess;
				tmp = Math.abs(esx - guess) / 2;
				guess -= (tmp == 0 ? 1 : tmp);
				neighbor.tell(OracleActor.Msg.TRY, getSelf());
				break;

			case MORE:
				esx = guess;
				tmp = Math.abs(edx - guess) / 2;
				guess += (tmp == 0 ? 1 : tmp);
				neighbor.tell(OracleActor.Msg.TRY, getSelf());
				break;

			case GUESSED:
				log("won!");
				neighbor.tell(OracleActor.Msg.LOSER, getSelf());
				getContext().stop(getSelf());
				break;

			case LOSER:
				log("sob!");
				neighbor.tell(OracleActor.Msg.LOSER, getSelf());
				getContext().stop(getSelf());
				break;

			case TRY:
				if(firstOneFlag)
					log("Turn: " + turn++);
				oracle.tell(guess, getSelf());
				break;
			}
		} else if (msg instanceof AssignMsg)
			oracle = ((AssignMsg) msg).getTarget();
		else if (msg instanceof NeighborMsg){
			neighbor = ((NeighborMsg) msg).getNeighbor();
			firstOneFlag = ((NeighborMsg) msg).getFirstFlag();
		}
		else
			unhandled(msg);
	}

	private void log(String msg) {
		System.out.println("[SmartPlayerActor-" + getSelf() + "] " + msg);
	}
}
