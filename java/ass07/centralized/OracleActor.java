package pap.ass07.centralized;

import java.util.Random;

import akka.actor.UntypedActor;

public class OracleActor extends UntypedActor {
	private Integer secret;

	public static enum Msg {
		GUESSED, LESS, MORE, LOSER, TRIED;
	}

	public void preStart() {
		secret = new Random(System.nanoTime()).nextInt();
		log("secret created");
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof Integer) {
			if (Integer.compare((int)msg, (int)secret) == 0){
				getSender().tell(Msg.GUESSED, getSelf());
				getContext().stop(getSelf());
			} else if (Integer.compare((int)msg, (int)secret) > 0){
				getSender().tell(Msg.LESS, getSelf());
			} else {
				getSender().tell(Msg.MORE, getSelf());
			}
		} else
			unhandled(msg);
	}

	private void log(String msg) {
		System.out.println("[OracleActor-" + getSelf() + "] " + msg);
	}
}
