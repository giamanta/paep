package pap.ass07.centralized;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class DumbPlayerActor extends UntypedActor {

	private Integer guess;
	private ActorRef arbiter;

	public void preStart() {
		log("ready to play");
		guess = 0;
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof OracleActor.Msg) {
			switch ((OracleActor.Msg) msg) {
			case TRIED:
				unhandled(msg);
				break;

			case LESS:
				guess--;
				this.arbiter.tell(OracleActor.Msg.TRIED, getSelf());
				break;

			case MORE:
				guess++;
				this.arbiter.tell(OracleActor.Msg.TRIED, getSelf());
				break;

			case GUESSED:
				log("won!");
				this.arbiter.tell(OracleActor.Msg.GUESSED, getSelf());
				getContext().stop(getSelf());
				break;

			case LOSER:
				log("sob!");
				getContext().stop(getSelf());
				break;
			}
		} else if (msg instanceof AssignMsg) {
			this.arbiter = getSender();
			((AssignMsg) msg).getTarget().tell(this.guess, getSelf());
		} else
			unhandled(msg);
	}

	private void log(String msg) {
		System.out.println("[DumbPlayerActor-" + getSelf() + "] " + msg);
	}
}
