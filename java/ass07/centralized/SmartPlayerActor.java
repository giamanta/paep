package pap.ass07.centralized;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class SmartPlayerActor extends UntypedActor {

	private Integer guess, edx, esx;
	private ActorRef arbiter;
	private int tmp;

	public void preStart() {
		log("ready to play");
		guess = 0;
		edx = Integer.MAX_VALUE;
		esx = Integer.MIN_VALUE;
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof OracleActor.Msg) {
			switch ((OracleActor.Msg) msg) {
			case TRIED:
				unhandled(msg);
				break;

			case LESS:
				edx = guess;
				tmp = Math.abs(esx - guess) / 2;
				guess -= (tmp == 0 ? 1 : tmp);
				this.arbiter.tell(OracleActor.Msg.TRIED, getSelf());
				break;

			case MORE:
				esx = guess;
				tmp = Math.abs(edx - guess) / 2;
				guess += (tmp == 0 ? 1 : tmp);
				this.arbiter.tell(OracleActor.Msg.TRIED, getSelf());
				break;

			case GUESSED:
				log("won!");
				this.arbiter.tell(OracleActor.Msg.GUESSED, getSelf());
				getContext().stop(getSelf());
				break;

			case LOSER:
				log("sob!");
				getContext().stop(getSelf());
				break;
			}
		} else if (msg instanceof AssignMsg) {
			this.arbiter = getSender();
			((AssignMsg) msg).getTarget().tell(this.guess, getSelf());
		} else
			unhandled(msg);
	}

	private void log(String msg) {
		System.out.println("[SmartPlayerActor-" + getSelf() + "] " + msg);
	}
}
