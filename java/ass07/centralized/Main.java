package pap.ass07.centralized;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class Main {
	public static void main(String[] args) {
		Random r = new Random(System.nanoTime());
		ArrayList<ActorRef> players = new ArrayList<ActorRef>();
		ActorSystem system = ActorSystem.create("GuessTheNumberCentralized");
		IntStream.range(0, Integer.parseInt(args[0])).forEach(
				i -> players.add(system.actorOf(
						Props.create(r.nextBoolean() ? SmartPlayerActor.class
								: DumbPlayerActor.class), "Players" + i)));
		ActorRef oracle = system.actorOf(Props.create(OracleActor.class),
				"Oracle");
		system.actorOf(Props.create(Arbiter.class, players, oracle), "Arbiter");
	}
}
