package pap.ass07.centralized;

import java.util.ArrayList;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.japi.Creator;

public class Arbiter extends UntypedActor {

	private ArrayList<ActorRef> players;
	private ActorRef oracle;
	private int currentPlay;
	private int turn;

	public Arbiter(ArrayList<ActorRef> players, ActorRef oracle) {
		this.players = players;
		this.oracle = oracle;
		this.currentPlay = 0;
		this.turn = 0;
	}

	public void preStart() {
		players.get(0).tell(new AssignMsg(oracle), getSelf());
	}

	/**
	 * Create Props for an actor of this type. It is a good idea to provide
	 * static factory methods on the UntypedActor which help keeping the
	 * creation of suitable Props as close to the actor definition as possible.
	 * This also allows usage of the Creator-based methods which statically
	 * verify that the used constructor actually exists instead relying only on
	 * a runtime check. Source : http://doc.akka.io/docs/akka
	 * /2.4-M1/java/untyped-actors.html#untyped-actors-java
	 *
	 * @param players
	 *            collection of all players
	 * @return a Props for creating this actor, which can then be further
	 *         configured (e.g. calling `.withDispatcher()` on it)
	 */
	public static Props props(final ArrayList<ActorRef> players,
			final ActorRef oracle) {
		return Props.create(new Creator<Arbiter>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Arbiter create() throws Exception {
				return new Arbiter(players, oracle);
			}
		});
	}

	public void subscribe(ActorRef newPlayer) {
		log(newPlayer + " subscribed");
		this.players.add(newPlayer);
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof OracleActor.Msg) {
			if (msg == OracleActor.Msg.TRIED) {
				if (++currentPlay == players.size()) {
					currentPlay = 0;
					log("Turn: " + turn++);
				}
				players.get(currentPlay).tell(new AssignMsg(oracle), getSelf());
			}
			if (msg == OracleActor.Msg.GUESSED){
				players.stream().forEach(p -> p.tell(OracleActor.Msg.LOSER, p));
				getContext().stop(getSelf());
			}
		}
	}

	private void log(String msg) {
		System.out.println("[Arbiter-" + getSelf() + "] " + msg);
	}
}
