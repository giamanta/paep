# Assignment 7 (20150521)

## Esercizio su Attori - "Guess the Number (again)"
```
Implementare il gioco "Guess the Number"  utilizzando il modello degli attori e
la tecnologia actor-based Akka.

Il gioco consiste in N attori Player (N parametro di ingresso
dell’applicazione) che devono indovinare un numero estratto a caso da un altro
attore, denominato Oracolo.

I player a turno devono tentare di indovinare il numero inviando all’Oracolo il
proprio guess. Nel caso in cui il guess corrisponda al  numero cercato, allora
l’Oracolo deve comunicare al player che ha inviato il guess che è il vincitore
e a tutti gli altri che hanno perso. Il vincitore deve quindi scrivere in
output "won!" e gli altri "sob!". Nel caso in cui il guess non corrisponda al
numero cercato, allora l’Oracolo deve comunicare al player che ha inviato il
guess un suggerimento, relativo al fatto che il numero specificato sia più
grande o più piccolo del numero cercato.

Ad ogni turno deve essere stampato in output il numero del turno. Il gioco
termina quando si trova un vincitore.
```
### Thoughts
I decided to implement two different types of players, a _smart_ one and a
_dump_ one. The behavior is similar, both send a number to the *Oracle* but the
smarter one use a more sophisticated technique used in attacks like blind-SQLi,
instead increment or decrement only. Guess time pass from linear (dumb version)
to logarithm (smart one).

The solution, after handle a bit with actors in akka, require to implement a
_protocol_ in order to sort players guess, at most one per turn.

Alternatives solutions are:

* centralized (is the *Oracle* that ask one player at a time the guess or a
  player supervisor that could tell to each player **who** the Oracle is with an
  _AssignMsg_)
* ring (like a linked list, each player know the next one, last player point to
  the first one in order to handle different turns) I dislike this solution, i
  prefer handle fault tolerance and players are for me the weakest chain
* exploit actor's mailbox, this is the solution that I have developed first and
  that I have thought the *more* elegant one. After continue reading the doc is
  not the way that akka recommends.
  [source(noMailBoxSize)](http://letitcrash.com/post/17707262394/why-no-mailboxsize-in-akka-2)
  I would like to check the mailbox size in order to let each player a fair turn
  but not only, the same number of hits per player should be the same! How to
  check if all players have send an hit? check mailbox size but its a wrong way.


#### Exploit actor's mailbox solution
1 [source(msgDelivery)](http://doc.akka.io/docs/akka/2.4-M1/general/message-delivery-reliability.html)
2 [source(mailboxes)](http://doc.akka.io/docs/akka/2.4-M1/java/mailboxes.html)
### Insights
```
The rule more specifically is that for a given pair of actors, messages sent
directly from the first to the second will not be received out-of-order. The
word directly emphasizes that this guarantee only applies when sending with the
tell operator to the final destination, not when employing mediators or other
message dissemination features (unless stated otherwise).

The guarantee is illustrated in the following:

 * Actor A1 sends messages M1, M2, M3 to A2

 * Actor A3 sends messages M4, M5, M6 to A2

   This means that:

  1. If M1 is delivered it must be delivered before M2 and M3
  2. If M2 is delivered it must be delivered before M3
  3. If M4 is delivered it must be delivered before M5 and M6
  4. If M5 is delivered it must be delivered before M6
  5. A2 can see messages from A1 interleaved with messages from A3
  6. Since there is no guaranteed delivery, any of the messages may be dropped,
     i.e. not arrive at A2


```
```
Akka implements a specific form called “parental supervision”. Actors can only
be created by other actors—where the top-level actor is provided by the
library—and each created actor is supervised by its parent. This restriction
makes the formation of actor supervision hierarchies implicit and encourages
sound design decisions. It should be noted that this also guarantees that actors
cannot be orphaned or attached to supervisors from the outside, which might
otherwise catch them unawares. In addition, this yields a natural and clean
shutdown procedure for (sub-trees of) actor applications.
```
[source(supervision)](http://doc.akka.io/docs/akka/2.4-M1/general/supervision.html)
### Resources
* http://akka.io/docs/
* [main2.4-M1(java)](http://doc.akka.io/docs/akka/2.4-M1/java.html)
* [actors model](http://c2.com/cgi/wiki?ActorsModel)
* [a crash course in failure](http://web.archive.org/web/20090430014122/http://nplus1.org/articles/a-crash-course-in-failure/)
* [reactive manifesto](http://www.reactivemanifesto.org/)
* [source(supervision)](http://doc.akka.io/docs/akka/2.4-M1/general/supervision.html)
* [source(msgDelivery)](http://doc.akka.io/docs/akka/2.4-M1/general/message-delivery-reliability.html)
  [source(noMailBoxSize)](http://letitcrash.com/post/17707262394/why-no-mailboxsize-in-akka-2)
