#Java 8

Stream seq di elementi che vogliamo elaborare con map, reduce, fold, filter
etc.. Sono da pensare come le liste che avevamo in Haskell.
For si può fare con iterete + limit (vedi slides)

## Thread Safety & Liveness
If we have to deal with stateless class, we

[?]Il fatto di avere risultati non deterministici causati da race condition, si
potrebbe usare questo "malfunzionamento" come seed per generatori di numeri
casuali?


## ass04

```
$ cd /tmp/workspace/ricci
$ ls
bin/ src/
$ javac -d bin/pap/ass04/ -sourcepath src src/pap/ass04/TextBall.java
$ cd bin/pap/ass04
$ java pap.ass04.TextBall 15
```

# Resources

[Main article oracle about Java 8 Stream](http://www.oracle.com/technetwork/articles/java/ma14-java-se-8-streams-2177646.html)
[drdobbs lambdas streams](http://www.drdobbs.com/jvm/lambdas-and-streams-in-java-8-libraries/240166818)

## Java
[Java Concurrency in Practice](http://jcip.net/)
[learning path](http://docs.oracle.com/javase/tutorial/tutorialLearningPaths.html)
[dev ref](http://docs.oracle.com/javase/8/docs/technotes/tools/unix/index.html)
[collections](http://docs.oracle.com/javase/8/docs/technotes/guides/collections/changes8.html)
