package pap.ass05;

/**
 *
 * 2-dimensional point
 * objects are completely state-less
 *
 */
public class P3d implements java.io.Serializable {

    /**
	 * Auto-generated serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	public double x,y,z;

	public P3d(){
		java.util.Random rand = new java.util.Random(System.currentTimeMillis());
		this.x = rand.nextDouble();
		this.y = rand.nextDouble();
		this.z = rand.nextDouble();
	}
    public P3d(double x,double y, double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }
    
    public double getX(){
    	return this.x;
    }
    
    public double getY(){
    	return this.y;
    }
    
    public double getZ(){
    	return this.z;
    }

    public String toString(){
        return "P3d("+x+","+y+")";
    }

}
