package pap.ass05;

public class Flag implements IFlag {

	private boolean flag = false;

	@Override
	public synchronized void setHigh() {
		flag = true;
	}

	@Override
	public synchronized void setLow() {
		flag = false;
	}
	
	@Override
	public synchronized boolean capture() {
		return flag;
	}

}
