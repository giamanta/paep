# Assignment 05 PAeP 2015

## Esercizi su interazione e coordinazione di processi
### [Java] 1
```
Data una collezione (array, lista,...) di punti P in uno spazio
tri-dimensionale e un punto C, si vuole determinare e stampare il punto P che
ha distanza euclidea minima da C, utilizzando un programma multi-threaded che
(1) massimizzi le performance,
(2) utilizzi solo meccanismi Java nativi per mutua esclusione/sincronizzazione.

Per testare il programma (in termini di performance), definire una collezione
di punti di dimensione opportuna (con punti definiti, ad esempio, in modo
casuale) e quindi verificare che le il programma sfrutti nel miglior modo
possibile tutti i processori presenti nel sistema HW in cui viene lanciato.

Confrontare la soluzione con una versione basata su stream Java (sequenziali).
```
First of all, we should make these classes:
* **P3d**, that represent our point;
* **Worker**, a super-class that extends Thread class in order to some debug prints
  * **WorkerEuclide**, sub-class that implements a method to calculate Euclide
    distance from two points. Its default constructure require a collection of
    points and another point C that will be tested against entire collection.
* **MinDistance**, main class that lunch our program. Inside it we must have a
  thread-safe variable, *mindi*. It should be checked and optionally modified by
  each thread, so we must ensure good concurrency paradigms.

#### Choosing the paradigm for mindi
My first thought goes to make a *syncronized* method that check if mindi is less
than each local thread's min value (called from Worker) and if so update its
value.

```java
  public static synchronized void setMin(double nmin) {
    mindi = mindi > nmin ? nmin : mindi;
  }
```
I dig deeper if there are faster approaches and I found AtomicReference.
From [docs.oracle.org](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/package-summary.html):
> An object reference that may be updated atomically. See the
> java.util.concurrent.atomic package specification for description of the
> properties of atomic variables.

Doing some tests, 80% of times is faster than *synchronized* method.

With a lot of points we could set the jvm memory like that:
```
 java pap.ass05.MinDistance -Xms800m -Xmx800m
 Worker0 from 0 to 750000
 Worker1 from 750000 to 1500000
 [i] Worker0 ended in 158 ms
 [i] Worker1 ended in 173 ms
 Min distance: 0.038705148756807896
 Total time: 173

```
**_NB_** If you want a speed up like your processors, you should have all data needed by
a thread in his private processor cache.
---

### [Java] 2 - Cooperative Team (Semafori)
```
Un team di 5 worker (w1..w5) lavorano concorrentemente condividendo 3 contatori
(c1..c3)  di classe UnsafeCounter classe (non thread-safe). In particolare:

  * W1 e W2 hanno il compito di incrementare rispettivamente c1 e c2,
    concorrentemente e ripetutamente.
  * W3 ha il compito di stampare il valore di c1 ogni volta che viene
    aggiornato da W1 e quindi di incrementare c3.
  * Analogamente W4 ha il compito di stampare il valore di c2 ogni volta che
    viene aggiornato da W2 e quindi di incrementare c3.
  * Infine W5 ha il compito di stampare il valore di c3 solo dopo che è stato
    incrementato sia da W3 che W4.
  * W1 e W2 possono procedere ad un nuovo incremento solo dopo che W5 ha
    stampato il valore di c3.

Implementare il programma multi-threaded in Java usando semafori come unico
meccanismo di coordinazione.
```
W1|W2|W3|W4|W5
:---:|:---:|:---:|:---:|:---:
incC1|incC2|waitW1|waitW2|wait34
wait5|wait5|printC1|printC2|printC3
||incC3|incC3|

---

### [Java] 3 -  Capture the Flag (Monitor)
```
Si vuole implementare un gioco caratterizzato dai seguenti elementi:

* N player, 1 Arbiter - componenti attivi

* 1 monitor Flag, 1 monitor Sync

Il monitor Flag che rappresenta una bandierina, che può essere in 2 stati:
alzata o abbassata. Ha come interfaccia tre metodi:

- void setHigh() - cambia lo stato in alzata

- void setLow()    - cambia lo stato in abbassata

- boolean capture() - restituisce true se la bandiera è alzata, false
altrimenti

Il thread Arbiter periodicamente (con periodo casuale) interagisce con il
monitor Flag cambiandone lo stato, tenendola alzata per un certo numero di
millisecondi (casuale).

I componenti attivi Player a turno cercano di catturare la flag chiamando il
metodo capture. La coordinazione fra i player - tale per cui si passano il
turno - deve essere realizzata implementando un opportuno monitor Sync, usato
dai Player.

Il monitor Sync deve avere la seguente interfaccia:

- void waitForTurn(int turn) - chiamata dal player i-esimo, sospende il player
fin quando non è il suo turno

- void next() - cede il turno al player successivo

Se un Player in un turno riesce a catturare la Flag, stampa: WON! e il suo
nome, altrimenti passa il turno. In tal caso, gli altri player devono stampare
“SOB” e il gioco termina.
```
---

## Ripasso su prog funzionale
### [Haskell] 4 - Prog funzionale in Haskell

```
Implementare una funzione Haskell print_nodes_at_dist che, dato un albero
binario t di interi e un valore intero d, stampa gli  elementi dell'albero la
cui distanza dal nodo radice è pari a d.

Esempio:

Supponendo che t sia il seguente albero binario: TODO

allora

- print_nodes_at_dist t 0
  stampa 6

- print_nodes_at_dist t 2
  stampa 4 9 7

- print_nodes_at_dist t 6
  non stampa nulla
```
Test could be made with
[**test-framework**](https://github.com/haskell/test-framework)
