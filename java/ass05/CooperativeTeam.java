package pap.ass05;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.stream.IntStream;

public class CooperativeTeam {

	private static ArrayList<UnsafeCounter> counters;
	private static ArrayList<Thread> workers;
	private static Semaphore c1inc, c2inc, c3inc, wait1, wait2, wait34;

	public static void main(String[] args) {
		workers = new ArrayList<Thread>();
		counters = new ArrayList<UnsafeCounter>();
		IntStream.range(0, 3).forEach(i -> counters.add(new UnsafeCounter(0)));

		c1inc = new Semaphore(1);
		c2inc = new Semaphore(1);
		c3inc = new Semaphore(1);

		wait1 = new Semaphore(0);
		wait2 = new Semaphore(0);
		wait34 = new Semaphore(0);

		Worker w1 = new Worker("W1") {
			public void run() {
				for (int i = 0; i < 25; i++) {
					try {
						inc1();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		};
		Worker w2 = new Worker("W2") {
			public void run() {
				for (int i = 0; i < 25; i++) {
					try {
						inc2();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		};
		Worker w3 = new Worker("W3") {
			public void run() {
				for (int i = 0; i < 25; i++) {
					try {
						wait1.acquire();
						printInfo("[W3] C1 is: " + counters.get(0).getValue());
						inc3();
						wait34.release();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		};
		Worker w4 = new Worker("W4") {
			public void run() {
				for (int i = 0; i < 25; i++) {
					try {
						wait2.acquire();
						printInfo("[W4] C2 is: " + counters.get(1).getValue());
						inc3();
						wait34.release();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		};
		Worker w5 = new Worker("W5") {
			public void run() {
				for (int i = 0; i < 25; i++) {
					try {
						wait34.acquire(2);
						printInfo("[W5] C3 is: " + counters.get(2).getValue()
								+ "\n");
						c1inc.release();
						c2inc.release();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		};
		workers.add(w1);
		workers.add(w2);
		workers.add(w3);
		workers.add(w4);
		workers.add(w5);

		for (Thread w : workers)
			w.start();
	}

	public static void inc1() throws InterruptedException {
		c1inc.acquire();
		counters.get(0).inc();
		wait1.release();
	}

	public static void inc2() throws InterruptedException {
		c2inc.acquire();
		counters.get(1).inc();
		wait2.release();
	}

	public static void inc3() throws InterruptedException {
		c3inc.acquire();
		counters.get(2).inc();
		c3inc.release();
	}

	public static void printInfo(String msg) {
		synchronized (System.out) {
			System.out.println(msg);
		}
	}
}
