package pap.ass05;

public class Sync implements ISync {

	private int currentTurn, turnsTot;

	public Sync(int currentTurn, int turnsTot) {
		this.currentTurn = currentTurn;
		this.turnsTot = turnsTot;
	}

	@Override
	public synchronized void waitForTurn(int turn) throws InterruptedException {
		while (turn != currentTurn)
			wait();
	}

	@Override
	public synchronized void next() {
		currentTurn = (currentTurn + 1) % turnsTot;
		notifyAll();
	}

}
