package pap.ass05;

import java.util.Collection;

public class WorkerEuclide extends Worker {
	Collection<P3d> collection;
	P3d p;
	long startTime;

	public WorkerEuclide(String name, Collection<P3d> c, P3d p, long startTime) {
		super(name);
		this.collection = c;
		this.p = p;
		this.startTime = startTime;
	}

	public void run() {
		MinDistance.setMin(this.collection.stream().parallel()
				.mapToDouble(p0 -> distance(p0, this.p)).min().getAsDouble());
		long dt = System.currentTimeMillis() - startTime;
		System.out
				.println("[i] " + super.getName() + " ended in " + dt + " ms");
	}

	double distance(P3d p0, P3d p1) {
		return Math.sqrt(Math.pow(p0.getX() - p1.getX(), 2)
				+ Math.pow(p0.getY() - p1.getY(), 2)
				+ Math.pow(p0.getZ() - p1.getZ(), 2));
	}
}
