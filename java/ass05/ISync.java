package pap.ass05;

public interface ISync {
	// called by player, suspend him until it's his turn
	void waitForTurn(int turn) throws InterruptedException;
	// leave turn to next player
	void next();
}