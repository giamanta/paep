package pap.ass05;

public interface IFlag {
	// raise the flag
	void setHigh();

	// lowering the flag
	void setLow();

	// true if flag raised otherwise false
	boolean capture();
}
