package pap.ass05;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class MinDistance2 {

	private static final int LEN = 150000;
	private static final P3d c = new P3d();
	private static final int howMany = Runtime.getRuntime()
			.availableProcessors();
	private static final int chunk = LEN / howMany;
	private static final int carry = LEN % howMany;

	// Min value
	static double mindi = Double.MAX_VALUE;

	public static void main(String[] args) {
		ArrayList<P3d> col = new ArrayList<P3d>();
		IntStream.range(0, LEN).forEach(i -> col.add(new P3d()));
		ArrayList<Thread> users = new ArrayList<Thread>();

		long t0 = System.currentTimeMillis(); // with +1

		IntStream.range(0, howMany).forEach(
				i -> {
					System.out.println("Worker"
							+ i
							+ " from "
							+ chunk
							* i
							+ " to "
							+ (i == howMany ? (chunk * i) + chunk + carry
									: chunk * i + chunk));
					WorkerEuclide we = new WorkerEuclide("Worker" + i, col
							.subList(chunk * i,
									(i == howMany ? (chunk * i) + chunk + carry
											- 1 : chunk * i + chunk - 1)), c,
							t0);
					users.add(we);
					we.start();
				});
		for (Thread user : users) {
			try {
				user.join();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		long t1 = System.currentTimeMillis();
		System.out.println("Min distance: " + mindi);
		System.out.println("Total time: " + (t1 - t0));
	}

	public static synchronized void setMin(double nmin) {
		mindi = mindi > nmin ? nmin : mindi;
	}
}
