package pap.ass05;

public abstract class Worker extends Thread {
	public Worker(String name){
		super(name);
	}

	protected void print(String msg){
		synchronized (System.out){
			System.out.print(msg);
		}
	}

	protected void println(String msg){
		synchronized (System.out){
			System.out.println(msg);
		}
	}
}
