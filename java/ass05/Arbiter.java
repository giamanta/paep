package pap.ass05;

import java.util.ArrayList;
import java.util.Random;

public class Arbiter extends Worker {

	private Flag f;
	private ArrayList<Player> players = new ArrayList<Player>();
	private boolean isOver = false;

	public Arbiter(String name, Flag f) {
		super(name);
		this.f = f;
	}

	public void run() {
		try {
			while (!isOver) {
				Thread.sleep(new Random(System.currentTimeMillis())
						.nextInt(500) + 50);
				f.setHigh();
				Thread.sleep(new Random(System.currentTimeMillis()).nextInt(50) + 5);
				f.setLow();
			}
		} catch (InterruptedException e) {
			isOver = true;
			for (Thread player : players)
				player.interrupt();
		}
	}

	public void subscribePlayer(Player p) {
		players.add(p);
	}

}