package pap.ass05;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class CaptureTheFlag {

	private static final int NPLAYER = 300;
	private static Flag f;
	private static Sync s;

	public static void main(String[] args) {
		f = new Flag();
		s = new Sync(1, NPLAYER);

		Arbiter a = new Arbiter("Arbiter", f);
		ArrayList<Player> ps = new ArrayList<Player>();

		IntStream.rangeClosed(1, NPLAYER).forEach(i -> {
			ps.add(new Player(i, a, f, s));
			a.subscribePlayer(ps.get(ps.size() - 1));
		});
		a.start();
		for(Thread player : ps)
			player.start();
	}
}
