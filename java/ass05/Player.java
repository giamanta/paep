package pap.ass05;

public class Player extends Worker {

	private int turn;
	private Flag monf;
	private Sync mons;
	private Arbiter arbiter;
	private boolean isOver;

	public Player(int turn, Arbiter a, Flag f, Sync s) {
		super("Player" + turn);
		this.turn = turn;
		this.arbiter = a;
		this.monf = f;
		this.mons = s;
	}

	public void run() {
		try {
			while (!this.isOver) {
				mons.waitForTurn(turn);
				if (monf.capture()) {
					println("WON! " + this.getName());
					this.isOver = true;
					this.arbiter.interrupt();
				} else {
					println(this.getName() + " miss");
					mons.next();
				}
			}
		} catch (InterruptedException e) {
			println(this.getName() + "SOB");
		}
	}
}
