package pap.lab41;

import java.util.Random;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class RandomActor extends UntypedActor {

	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	private int value;
	
	public void preStart() {
		value = new Random(System.currentTimeMillis()).nextInt();
	}

	@Override
	public void onReceive(Object arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
