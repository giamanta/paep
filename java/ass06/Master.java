package pap.ass06;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

public class Master extends Thread {

	private final static int nTasks = Runtime.getRuntime()
			.availableProcessors() + 1;
	private GameOfLifeView view;
	private GameOfLife game;
	private Flag stopFlag;
	private int npoints;
	private int dx;
	private Set<Future<Integer>> resultSet;
	private ExecutorService executor;
	private long t0, t1;
	private int flux;

	public Master(GameOfLife game, GameOfLifeView view, Flag stopFlag) {
		this.game = game;
		this.view = view;
		this.stopFlag = stopFlag;
		this.npoints = game.getCells().length;
		this.dx = npoints / nTasks;
	}

	public void run() {
		executor = Executors.newFixedThreadPool(nTasks);
		resultSet = new HashSet<Future<Integer>>();
		while (!stopFlag.isSet()) {
			try {

				flux = 0;
				resultSet.clear();
				t0 = System.currentTimeMillis();
				view.changeState("Processing...");
				for (int i = 1, x0 = 0; i <= nTasks; i++, x0 += dx) {
					Future<Integer> res = executor
							.submit(new ComputeStripeTask(x0,
									(i == nTasks) ? npoints : x0 + dx, game,
									stopFlag));
					resultSet.add(res);
					log("[Master] submitted task " + x0 + " " + (x0 + dx));
				}

				// TODO:Stream!
				for (Future<Integer> future : resultSet)
					flux += future.get().intValue();

				game.updateState(flux);
				view.setUpdated(game);

				if (!stopFlag.isSet()) {
					t1 = System.currentTimeMillis();
					view.changeState("Computation done, elapsed: " + (t1 - t0)
							+ "ms");
				} else
					view.changeState("interrupted");
				Thread.sleep(60);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private void log(String msg) {
		synchronized (System.out) {
			System.out.println(msg);
		}
	}

}
