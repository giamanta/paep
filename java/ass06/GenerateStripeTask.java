package pap.ass06;

import java.util.concurrent.Callable;

public class GenerateStripeTask implements Callable<Integer> {

	private GameOfLife result;
	private int from;
	private int to;
	private Flag stopFlag;

	public GenerateStripeTask(int from, int to, GameOfLife result, Flag stopFlag) {
		this.result = result;
		this.from = from;
		this.to = to;
		this.stopFlag = stopFlag;
	}

	@Override
	public Integer call() throws Exception {
		int flux = 0;
		log("[Genera] start generating");
		int nSteps = 4;
		int dx = (to - from) / nSteps;
		for (int i = 1; i <= nSteps; i++, from += dx) {
			if (stopFlag.isSet())
				break;
			flux += result.generateSlice(from, (i == nSteps) ? to : from + dx);
		}
		if (!stopFlag.isSet())
			log("[Genera] generation completed");
		else
			log("[Genera] generation interrupted");
		return flux;
	}

	private void log(String msg) {
		synchronized (System.out) {
			System.out.println(msg);
		}
	}

}
