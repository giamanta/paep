package pap.ass06;

import java.util.Arrays;
import java.util.Random;

import pap.ass06.interfaces.IBoard;
import pap.ass06.interfaces.IShape;

public class GameOfLife implements IBoard {

	private final static int LIVE = 0xffffffff;
	private final static int DEAD = 0xff000000;

	private IShape borders;
	private int[] cells;
	private int[] newCells;
	// private HashMap<Integer, Boolean> toCheck;
	private int liveCells;
	private int decade;
	private Random r;

	public GameOfLife(IShape b) {
		setPerimeter(b);
		this.cells = new int[b.getNPixels()];
		this.newCells = new int[b.getNPixels()];
		Arrays.fill(this.newCells, DEAD);
		this.liveCells = 0;
		this.decade = 0;
		r = new Random();
		r.setSeed(System.currentTimeMillis());
	}

	@Override
	public void setPerimeter(IShape borders) {
		this.borders = borders;
	}

	@Override
	public IShape getPerimeter() {
		return this.borders;
	}

	@Override
	public void setCells(int[] cells) {
		this.cells = cells;
	}

	@Override
	public int[] getCells() {
		return this.cells;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pap.ass06.interfaces.IBoard#computeSlice(int, int)
	 */
	@Override
	public int computeSlice(int from, int to) {
		int flux = 0;
		for (int i = from; i < to; i++) {
			// 1) una cella m[i,j] che nello stato s(t) è live e ha zero o al
			// più una
			// cella vicina live (e le altre dead), nello stato s(t+1) diventa
			// dead
			// (“muore di solitudine”)
			// 2) una cella m[i,j] che nello stato s(t) è live e ha quattro o
			// più celle
			// vicine live, nello stato s(t+1) diventa dead (“muore di
			// sovrappopolamento”)
			// 3) una cella m[i,j] che nello stato s(t) è live e ha due o tre
			// celle
			// vicine live, nello stato s(t+1) rimane live (“sopravvive”)
			// 4) una cella m[i,j] che nello stato s(t) è dead e ha tre celle
			// vicine
			// live, nello stato s(t+1) diventa live
			int nn = this.getNeighbours(i);
			if (this.cells[i] == LIVE) { // prev live
				if (nn <= 1 || nn >= 4) {
					this.newCells[i] = DEAD;
					flux--;
				} else
					// if prev live and current live, flux still the same
					this.newCells[i] = LIVE;
			} else { // prev DEAD
				if (nn == 3) {
					this.newCells[i] = LIVE;
					flux++;
				}// if prev dead and current dead, flux still the same
				else
					this.newCells[i] = DEAD;
			}
		}
		return flux;
	}

	public int getNeighbours(int who) {
		int n = 0, row = 0, column = 0;
		// We know that ATM only Rectangle are supported
		if (this.borders instanceof Rect) {
			int lineLen = (int) this.borders.linesLen();
			final int MAXCOLUMN = lineLen - 1; // NB: -1 -> 0 based
			final int MAXROW = this.borders.lines() - 1; // NB: -1 -> 0 based
			row = who / lineLen;
			column = who % lineLen;

			if (column < MAXCOLUMN) // Right
				if (this.cells[who + 1] == LIVE)
					n++;
			if (column > 0) // Left
				if (this.cells[who - 1] == LIVE)
					n++;
			if (row > 0) // Upper
				if (this.cells[who - lineLen] == LIVE)
					n++;
			if (row < MAXROW) // Lower
				if (this.cells[who + lineLen] == LIVE)
					n++;
			if (row > 0 && column < MAXCOLUMN) // Upper right
				if (this.cells[who + 1 - lineLen] == LIVE)
					n++;
			if (row < MAXROW && column < MAXCOLUMN) // Lower right
				if (this.cells[who + 1 + lineLen] == LIVE)
					n++;
			if (row > 0 && column > 0) // Upper left
				if (this.cells[who - 1 - lineLen] == LIVE)
					n++;
			if (row < MAXROW && column > 0) // Lower left
				if (this.cells[who - 1 + lineLen] == LIVE)
					n++;
		}
		return n;
	}

	/**
	 * Generate the 1st generation at Random, thread safe
	 * 
	 * @param from
	 *            Starting Index inclusive
	 * @param to
	 *            Ending Index exclusive
	 */
	public int generateSlice(int from, int to) {
		int flux = 0;
		for (int i = from; i < to; i += r.nextInt(8) + 1) {
			boolean color = r.nextBoolean();
			if (color) {
				flux++;
				this.newCells[i] = LIVE;
			}
		}
		return flux;
	}

	public int getLives() {
		return liveCells;
	}

	public int getDecade() {
		return decade;
	}

	public void updateState(int sum) {
		System.arraycopy(this.newCells, 0, this.cells, 0, this.borders.getNPixels());
		this.liveCells = this.liveCells + sum % Integer.MAX_VALUE;
		this.decade = (this.decade + 1) % Integer.MAX_VALUE;
	}

}
