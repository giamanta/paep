package pap.ass06;

import pap.ass06.interfaces.IInputListener;

public class Controller implements IInputListener {

	private GameOfLife game;
	private GameOfLifeView view;
	private Flag stopFlag;

	public Controller(GameOfLife game, GameOfLifeView view) {
		this.game = game;
		this.view = view;
	}

	@Override
	public void started() {
		stopFlag.reset();
		new Master(game, view, stopFlag).start();
	}

	@Override
	public void stopped() {
		stopFlag.set();
	}

	@Override
	public void generate() {
		stopFlag = new Flag();
		this.game = new GameOfLife(game.getPerimeter());
		new Generator(game, view, stopFlag).start();
	}

}
