package pap.ass06.interfaces;

public interface IInputListener {

	public void started();
	
	public void stopped();
	
	public void generate();
}
