package pap.ass06.interfaces;

public interface IBoard {

	public void setPerimeter(IShape borders);

	public IShape getPerimeter();

	public void setCells(int[] cells);

	public int[] getCells();
	
	/**
	 * @param from inclusive
	 * @param to exclusive
	 * @return # of live cells
	 */
	public int computeSlice(int from, int to);

}
