package pap.ass06.interfaces;

import pap.ass06.GameOfLife;

public interface IGameOfLifeListener {

	public void update(GameOfLife game);
}
