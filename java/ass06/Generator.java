package pap.ass06;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Generator extends Thread {

	private GameOfLifeView view;
	private GameOfLife game;
	private Flag stopFlag;

	public Generator(GameOfLife game, GameOfLifeView view, Flag stopFlag) {
		this.game = game;
		this.view = view;
		this.stopFlag = stopFlag;
	}

	public void run() {
		try {
			int nTasks = Runtime.getRuntime().availableProcessors() + 1;
			int npoints = game.getCells().length;
			int dx = npoints / nTasks;
			Set<Future<Integer>> resultSet = new HashSet<Future<Integer>>();
			long t0 = System.currentTimeMillis();
			ExecutorService executor = Executors.newFixedThreadPool(nTasks);

			view.changeState("Init the image...");
			for (int i = 1, x0 = 0; i <= nTasks; i++, x0 += dx) {
				Future<Integer> res = executor.submit(new GenerateStripeTask(
						x0, (i == nTasks) ? npoints : x0 + dx, game, stopFlag));
				resultSet.add(res);
				log("submitted task " + x0 + " " + (x0 + dx));
			}

			int sum = 0;
			for (Future<Integer> future : resultSet)
				sum += future.get();

			executor.shutdown();
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
			game.updateState(sum);
			view.setUpdated(game);

			if (!stopFlag.isSet()) {
				long t1 = System.currentTimeMillis();
				view.changeState("Generation done, elapsed: " + (t1 - t0)
						+ "ms");
			} else
				view.changeState("interrupted");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void log(String msg) {
		synchronized (System.out) {
			System.out.println(msg);
		}
	}

}
