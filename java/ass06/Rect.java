package pap.ass06;

import pap.ass06.interfaces.*;

/**
 * classe per la rappresentazione di un rettangolo
 *
 * @author giacomo mantani
 */
public class Rect implements IShape {
	P2d pa, pb;
	int pixels; // # of pixel inside
	int lineLen;
	int lines;

	public Rect(P2d pa, P2d pb) {
		super();
		this.pa = pa;
		this.pb = pb;
		pixels = (this.pb.getX() - this.pa.getX())
				* (this.pb.getY() - this.pa.getY());
		lineLen = this.pb.getX() - this.pa.getX();
		lines = this.pb.getY() - this.pa.getY();
	}

	@Override
	public void move(V2d v) {
		this.pa = new P2d(this.pa.getX() + v.getX(), this.pa.getY() + v.getY());
		this.pb = new P2d(this.pb.getX() + v.getX(), this.pb.getY() + v.getY());
	}

	@Override
	public boolean contains(P2d p) {
		return (p.getX() <= this.pb.getX() && p.getY() <= this.pb.getY())
				&& (p.getX() >= this.pa.getX() && p.getY() >= this.pa.getY());
	}

	@Override
	public boolean isInside(BBox bbox) {
		return (bbox.getUpperLeft().getX() <= this.pa.getX() && bbox
				.getUpperLeft().getY() <= this.pa.getY())
				&& (bbox.getBottomRight().getX() >= this.pb.getX() && bbox
						.getBottomRight().getY() >= this.pb.getY());
	}

	@Override
	public int getNPixels() {
		return this.pixels;
	}

	@Override
	public BBox getBBox() {
		return new BBox(this.pa, this.pb);
	}

	@Override
	public String toString() {
		return "Rect pa(" + this.pa.getX() + "," + this.pa.getY() + ") pb("
				+ this.pb.getX() + "," + this.pb.getY() + ")";
	}

	@Override
	public Integer linesLen() {
		return this.lineLen;
	}

	@Override
	public int lines() {
		return lines;
	}

}
