package pap.ass06;

public class GameOfLiveViewer {

	public static void main(String[] args) {

		int w = 999;
		int h = 555;

		GameOfLife model = new GameOfLife(new Rect(new P2d(0, 0), new P2d(w, h)));
		GameOfLifeView view = new GameOfLifeView(w, h);
		Controller controller = new Controller(model, view);
		view.addListener(controller);
		view.setVisible(true);
	}
}