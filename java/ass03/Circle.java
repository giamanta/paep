package pap.ass03;

/**
 * classe per la rappresentazione di un cerchio
 *
 * @author giacomo mantani
 */
public class Circle implements Shape {

	P2d c;
	Float r;

	public Circle(P2d c, Float r) {
		super();
		this.c = c;
		this.r = r;
	}

	@Override
	public void move(V2d v) {
		c = new P2d(c.getX() + v.getX(), c.getY() + v.getY());
	}

	@Override
	public double getPerim() {
		return 2 * Math.PI * r;
	}

	@Override
	public boolean contains(P2d p) {
		return Utils.distance(p, c) <= r;
	}

	@Override
	public boolean isInside(BBox bbox) {
		BBox current = getBBox();
		return (bbox.getUpperLeft().getX() <= current.getUpperLeft().getX() && bbox.getUpperLeft()
				.getY() <= current.getUpperLeft().getY())
				&& (bbox.getBottomRight().getX() >= current.getBottomRight().getX() && bbox.getBottomRight()
						.getY() >= current.getBottomRight().getY());
	}

	@Override
	public BBox getBBox() {
		return new BBox(new P2d(c.getX() - Math.round(r), c.getY()
				- Math.round(r)), new P2d(c.getX() + Math.round(r), c.getY()
				+ Math.round(r)));
	}

	@Override
	public String toString() {
		return "Circle (" + this.c.getX() + "," + this.c.getY() + ") r:" + this.r;
	}

}
