package pap.ass03;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TestShapeViewer {
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
                createAndShowGUI(); 
			}
		});
	}
    private static void createAndShowGUI() {
		List<Shape> shapes = new ArrayList<Shape>();
		shapes.add(new Line(new P2d(50,5), new P2d(100,5)));
		shapes.add(new Rect(new P2d(8,8), new P2d(25,25)));
		shapes.add(new Rect(new P2d(75,55), new P2d(113,93)));
		shapes.add(new Circle(new P2d(50,50), (float)15));
		List<Shape> cs = new ArrayList<Shape>();
		cs.add(new Circle(new P2d(80,80), (float)8));
		cs.add(new Circle(new P2d(90,80), (float)8));
		cs.add(new Circle(new P2d(100,80), (float)8));
		cs.add(new Circle(new P2d(100,70), (float)8));
		cs.add(new Circle(new P2d(90,60), (float)8));
		shapes.add(new Combo(cs));
        JFrame f = new JFrame("PAeP assignement 3");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Viewer v = new Viewer();
        v.update(shapes);
        f.add(v);
        f.pack();
        f.setVisible(true);
    }
}
