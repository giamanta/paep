package pap.ass03;

import java.util.ArrayList;
import java.util.List;

public class TestShapes {
	public static void main(String[] args) {
		List<Shape> shapes = new ArrayList<Shape>();
		List<Shape> tmp = new ArrayList<Shape>();
		shapes.add(new Line(new P2d(50, 5), new P2d(100, 5)));
		shapes.add(new Rect(new P2d(8, 8), new P2d(25, 25)));
		shapes.add(new Circle(new P2d(50, 50), (float) 15));
		List<Shape> cs = new ArrayList<Shape>();
		cs.add(new Circle(new P2d(80, 80), (float) 8));
		cs.add(new Circle(new P2d(90, 80), (float) 8));
		cs.add(new Circle(new P2d(100, 80), (float) 8));
		cs.add(new Circle(new P2d(100, 70), (float) 8));
		cs.add(new Circle(new P2d(90, 60), (float) 8));
		shapes.add(new Combo(cs));
		tmp = shapes;

		System.out.println("Print main list: ");
		Utils.logAll(shapes);

		System.out .println("\nPrint from main list only shapes that contains P2d(52,52): ");
		tmp = Utils.getContaining(shapes, new P2d(52, 52));
		Utils.logAll(tmp);
		tmp = shapes;

		System.out.println("\nPrint main list moved by V2d(15,15): ");
		Utils.moveShapes(shapes, new V2d(15, 15));
		Utils.logAll(shapes);

		System.out.println("\nShape with max perim: "
				+ Utils.shapeWithMaxPerim(shapes) + " value:"
				+ Utils.maxPerim(shapes));

		System.out.println("\nPrint main list with shapes sorted by X: ");
		shapes = Utils.sortShapesByX(shapes);
		Utils.logAll(shapes);

		System.out.println("\nPrint main list in BBox( (75,75),  (140,140)): ");
		shapes = Utils.inBBox(shapes, new BBox(new P2d(75, 75), new P2d(140,
				140)));
		Utils.logAll(shapes);
	}
}
