package pap.ass03;

import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class Utils {

	/*
	 * data una lista di figure e un vettore dv, trasla ogni figura del vettore
	 * specificato
	 */
	static void moveShapes(List<Shape> l, V2d dv) {
		l.forEach(s -> s.move(dv));
	}

	/*
	 * data una lista di figure e una coppia di vertici p0 p1, computa la lista
	 * delle figure contenute nel bounding box p0 p1
	 */
	static List<Shape> inBBox(List<Shape> l, BBox bbox) {
		return l.stream().filter(s -> s.isInside(bbox)).collect(toList());
	}

	/* data una lista di figure, determina il perimetro maggiore */
	static double maxPerim(List<Shape> l) {
		return l.stream().mapToDouble(Shape::getPerim).max().orElse(-1);
	}

	/* data una lista di figure, determina la figura con perimetro maggiore */
	static Shape shapeWithMaxPerim(List<Shape> l) {
		return l.stream().max(comparing(Shape::getPerim)).get();
	}

	/* data una lista di figure, le ordina lungo l'asse x */
	static List<Shape> sortShapesByX(List<Shape> l) {
		return l.stream()
				.sorted((a, b) -> Integer.compare(a.getBBox().getUpperLeft().getX(),
						b.getBBox().getUpperLeft().getX())).collect(toList());
	}

	/*
	 * data una lista di figure e un punto, verifica se esiste una figura che
	 * contiene il punto
	 */
	static boolean contains(List<Shape> l, P2d p) {
		return l.stream().anyMatch(s -> s.contains(p));
	}

	/*
	 * data una lista di figure e un punto p, computa la lista delle figure che
	 * contengono il punto
	 */
	static List<Shape> getContaining(List<Shape> l, P2d p) {
		return l.stream().filter(s -> s.contains(p) == true).collect(toList());
	}

	/* data una lista di figure, le stampa in uscita */
	static void logAll(List<Shape> l) {
		l.forEach(System.out::println);
	}

	static double distance(P2d p0, P2d p1) {
		return Math.sqrt(Math.pow(p0.getX() - p1.getX(), 2)
				+ Math.pow(p0.getY() - p1.getY(), 2));
	}

}
