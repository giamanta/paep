package pap.ass03;

/**
 * classe per la rappresentazione di una linea
 *
 * @author giacomo mantani
 */
public class Line implements Shape {

	P2d pa, pb;

	public Line(P2d pa, P2d pb) {
		super();
		this.pa = pa;
		this.pb = pb;
	}

	@Override
	public void move(V2d v) {
		this.pa = new P2d(this.pa.getX() + v.getX(), this.pa.getY() + v.getY());
		this.pb = new P2d(this.pb.getX() + v.getX(), this.pb.getY() + v.getY());
	}

	@Override
	public double getPerim() {
		return Utils.distance(this.pa, this.pb);
	}

	@Override
	public boolean contains(P2d p) {
		int dx = this.pb.getX() - this.pa.getX();
		int dy = this.pb.getY() - this.pa.getY();
		float m = dy / (dx == 0 ? 1 : dx);
		return Math.abs(p.getY()
				- (m * p.getX() + (this.pa.getY() - m * this.pa.getX()))) < 0.001f;
	}

	@Override
	public boolean isInside(BBox bbox) {
		BBox current = getBBox();
		return (bbox.getUpperLeft().getX() <= current.getUpperLeft().getX() && bbox.getUpperLeft()
				.getY() <= current.getUpperLeft().getY())
				&& (bbox.getBottomRight().getX() >= current.getBottomRight().getX() && bbox.getBottomRight()
						.getY() >= current.getBottomRight().getY());
	}

	@Override
	public String toString() {
		return "Line pa(" + this.pa.getX() + "," + this.pa.getY() + ") pb("
				+ this.pb.getX() + "," + this.pb.getY() + ")";
	}

	@Override
	public BBox getBBox() {
		return new BBox(new P2d(Math.min(this.pa.getX(), this.pb.getX()),
				Math.min(this.pa.getY(), this.pb.getY())), new P2d(Math.max(
				this.pa.getX(), this.pb.getX()), Math.max(this.pa.getY(),
				this.pb.getY())));
	}

}
