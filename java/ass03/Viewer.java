package pap.ass03;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Viewer extends JPanel implements ShapeViewer {

	private static final long serialVersionUID = 1L;
	List<Shape> shapes;

	public Viewer() {
		setBorder(BorderFactory.createLineBorder(Color.black));

		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				update(Utils.getContaining(shapes, new P2d(e.getX(), e.getY())));
			}
		});
	}

	public Dimension getPreferredSize() {
		return new Dimension(550, 400);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Random r = new Random();
		g.setColor(new Color(r.nextInt(250)+1));
		this.shapes.forEach(s -> drawShape(s, g));
	}

	public void drawShape(Shape s, Graphics g) {
		if (s instanceof Line)
			g.drawLine(((Line) s).pa.getX(), ((Line) s).pa.getY(),
					((Line) s).pb.getX(), ((Line) s).pb.getY());
		else if (s instanceof Rect)
			g.drawRect(((Rect) s).pa.getX(), ((Rect) s).pa.getY(),
					(int)Math.round(Utils.distance(((Rect) s).pa, new P2d(((Rect) s).pb.getX(), ((Rect) s).pa.getY()))), 
					(int)Math.round(Utils.distance(((Rect) s).pa, new P2d(((Rect) s).pa.getX(), ((Rect) s).pb.getY()))));
		else if (s instanceof Circle)
			g.drawOval(((Circle) s).c.getX(), ((Circle) s).c.getY(),
					Math.round(((Circle) s).r), Math.round(((Circle) s).r));
		else if (s instanceof Combo)
			((Combo) s).ls.stream().forEach(shape -> drawShape(shape, g));
	}

	@Override
	public void update(List<Shape> shapes) {
		this.shapes = shapes;
		repaint();
	}

}
