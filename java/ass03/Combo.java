package pap.ass03;

import java.util.List;
import java.util.stream.Collectors;

/**
 * classe per la rappresentazione di un insieme di figure
 *
 * @author giacomo mantani
 */
public class Combo implements Shape {

	List<Shape> ls;

	public Combo(List<Shape> l) {
		this.ls = l;
	}

	@Override
	public void move(V2d v) {
		Utils.moveShapes(this.ls, v);
	}

	@Override
	public double getPerim() {
		return ls.stream().mapToDouble(Shape::getPerim)
				.reduce(0, (a, b) -> a + b);
	}

	@Override
	public boolean contains(P2d p) {
		return ls.stream().anyMatch(s -> s.contains(p));
	}

	@Override
	public boolean isInside(BBox bbox) {
		return ls.stream().anyMatch(s -> s.isInside(bbox));
	}

	@Override
	public BBox getBBox() { // global one (contains all combo figures)
		BBox minx = ls.stream().map(Shape::getBBox)
				.reduce((a, b) -> a.getUpperLeft().getX() < b.getUpperLeft().getX() ? a : b)
				.get();
		BBox miny = ls.stream().map(Shape::getBBox)
				.reduce((a, b) -> a.getUpperLeft().getY() < b.getUpperLeft().getY() ? a : b)
				.get();
		BBox maxx = ls
				.stream()
				.map(Shape::getBBox)
				.reduce((a, b) -> a.getBottomRight().getX() > b.getBottomRight().getX() ? a
						: b).get();
		BBox maxy = ls
				.stream()
				.map(Shape::getBBox)
				.reduce((a, b) -> a.getBottomRight().getY() > b.getBottomRight().getY() ? a
						: b).get();
		return new BBox(new P2d(minx.getUpperLeft().getX(), miny.getUpperLeft().getY()),
				new P2d(maxx.getBottomRight().getX(), maxy.getBottomRight().getY()));
	}

	@Override
	public String toString() {
		return "{" + this.ls.stream().map(Object::toString).collect(Collectors.joining(",")) + "}";
	}

}
