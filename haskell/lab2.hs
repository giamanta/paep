import Screen

type Word = String

mapLen :: [Word] -> [(Word,Int)]
mapLen xs = map (\s -> (s, length s)) xs

selectedLen :: [Word] -> Int -> [(Word,Int)]
selectedLen xs w =
  filter (\(_,l) -> l > w) (map (\p -> (p, length p)) xs)

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter pred' (x:xs)
  | pred' x    = x : myfilter pred' xs
  | otherwise = myfilter pred' xs

wordOcc :: [Word] -> Word -> Int
wordOcc xs w = foldr (\p s -> if (p==w) then s + 1 else s) 0 xs

wordInfo :: [String] -> [(String,Int)]
wordInfo xs = map (\x -> (x, length x)) xs

addInfo :: (String,Int) -> [(Int,[String])] -> [(Int,[String])]
addInfo (s, l) [] = [(l,[s])]
addInfo (s, l) ((l1,ws):xs)
    | l == l1 = (l1,ws++[s]):xs
    | otherwise = (l1,ws) : (addInfo (s, l) xs)

wordsOcc :: [String] -> [(Int,[String])]
wordsOcc xs = foldr addInfo [] (wordInfo xs)

type TraderId = String

data Trader = Trader TraderId String
    deriving (Show)

data Transaction = Trans TraderId Int Int
    deriving (Show)

traders = [
Trader "john" "Cambridge",
Trader "mario" "Milan",
Trader "alan" "Cambridge",
Trader "andrea" "Cesena"]

transactions = [
    Trans "andrea" 2011 300,
    Trans "john" 2012 1000,
    Trans "john" 2011 400,
    Trans "mario" 2012 710,
    Trans "mario" 2011 700,
    Trans "alan" 2012 950]

{-
Esprimere mediante composizione di funzioni le seguenti espressioni,
considerando traders e transactions come sorgenti di partenza:

    la lista delle transazioni riferite all’anno 2011, ordinate per valore
    l’elenco delle città distinte dove operano i trader
    se esistono trader da Milano
    il valore massimo delle transazioni
    la transazione con il valore minimo
    stampa tutti i valori delle transazioni di trader che abitano a Cambridge
-}
-- la lista delle transazioni riferite ad un certo anno, ordinate per valore
queryYear list whatYear =
        sort (filter (\(Trans _ year _) -> year == whatYear) list) (\(Trans _ _ value) -> value )

sort :: (Ord b) =>  [a] -> (a -> b) -> [a]
sort [] _ = []
sort (x:xs) f =
    sort [y | y <- xs, f y <= f x] f ++ [x] ++ sort [z | z <- xs, f z > f x] f

-- elenco città distinte dove operano i trader
queryDistinctCity list =
    foldr (\city lst -> if (member city lst) then lst else (city:lst)) []
         (map (\(Trader _ city) -> city) list)

member :: (Eq a) => a -> [a] -> Bool
member _ [] = False
member y (x:xs)
    | y == x = True
    | otherwise = member y xs

-- esistono trader da una certa città
queryCityTrader list city = foldr (||) False (map (\(Trader _ cty) -> cty == city) list)
-- il valore massimo delle transazioni
queryMaxTransValue list =
     foldr (\(Trans _ _ v) currmax -> if (v > currmax) then v else currmax) 0 list
-- la transazione con il valore minimo
queryMaxTrans list = head reslist
  where
    reslist = filter (\(Trans _ _ v) -> v == maxValue) list
    maxValue = foldr (\(Trans who yr v) currmax -> if (v > currmax) then v else currmax) 0 list

-- stampa tutti i valori delle transazioni di trader che abitano a Cambridge
printAllTransOfTraderFromCity trans traders city =
    foldr (\(Trans _ _ v) act -> (putStrLn (show v)) >> act) (return ()) selTrans
    where
            tradersInCity = filter (\(Trader id cty) -> cty == city) traders
            tradersName = map (\(Trader id _) -> id) tradersInCity
            selTrans = filter (\(Trans id _ value) -> member id tradersName) trans

-- Implementare una funzione che data una lista di elementi, restituisce
-- un’azione il cui effetto è di stampare in uscita tutti gli elementi, uno per
-- linea,  indentati di 4 posizioni.
printIndented :: (Show a) => [a] -> IO ()
printIndented xs = foldr (\s act -> putStrLn("        "++show s) >> act) (return ()) xs
