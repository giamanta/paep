#Haskell

Haskell ha scelto di minimizzare il numero di separatori sintattici per
scrivere il vostro programma. Ad esempio non abbiamo bisogno di ';' cercando di
avere il tutto gratis mediante una definizione molto leggibile usando bene il
layout della frase. Il ';' può essere usato se si vuole scrivere più definizioni
sulla stessa riga. Altrimenti è il tab a far da padrone.

Tutte le volte input lista e output stessa dimensione, _map_.
Tutte le volte input una lista che in output restituisce una lista con lunghezza
diversa, _fold\*_.

Moduli, Nome con lettera maiuscola, e generalmente con lo stesso nome deve
essere il file.

_getChar_ >>= _putChar_ sono ottime per comporle con il binding!

## IDE
In order to use haskell a bit more faster, I've added Vim command **make** see
`.vimrc`.

### Names and binding
_Ricorda_ che l'applicazione ha la priorità maggiore.
  I nomi con le maiuscole per _tipi_, _data constructor_, _moduls name_ (es True
  e False). Le variabile minuscole anche le _type variable_ (definite nei tipi
  delle funzioni.)

https://wiki.haskell.org/99_questions/Solutions/7

# Resources

[elearning-cds](https://elearning-cds.unibo.it/course/view.php?id=4063)

[coding style](http://courses.cms.caltech.edu/cs11/material/haskell/misc/haskell_style_guide.html)

[awesome slides](http://shuklan.com/haskell/)

[haskell.org resources](https://www.haskell.org/documentation)

[Gentle intro 98 code](https://www.haskell.org/tutorial/code/)

[cis194 lectures](https://www.seas.upenn.edu/~cis194/lectures.html)

[99-haskell problem](https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems)

[Programming in Haskell book resources](http://www.cs.nott.ac.uk/~gmh/book.html)

[Google I/O 2012 - Go Concurrency Patterns](http://youtu.be/f6kdp27TYZs)

[Rob Pyke slides concurrency vs parallelism (Go samples)][rp]

[Bell Labs and CSP Threads](http://swtch.com/~rsc/thread/)

[Dijkstra](http://www.cs.utexas.edu/users/EWD/)

[Haskell Hard Way](http://yannesposito.com/Scratch/en/blog/Haskell-the-Hard-Way/)

[Problems](http://www.spoj.com/problems/TEST/)

[Tons](https://haskell.zeef.com/konstantin.skipor)

## Slam a ride

### Reading about concurrency
[A Concurrent Window System - Rob Pyke](http://swtch.com/~rsc/thread/cws.pdf)

[Interpreting the Data: Parallel Analysis with Sawzall](http://research.google.com/archive/sawzall.html)

[Parallelism is not concurrency][pnc]

[Unix oral history project](http://www.princeton.edu/~hos/Mahoney/expotape.htm)

### From GHC developers

[Parallelism /= Concurrency][pcghc]

### Fun with haskell

[literate programming](https://wiki.haskell.org/Literate_programming)


[pcghc]:https://ghcmutterings.wordpress.com/2009/10/06/parallelism-concurrency/
[pnc]:https://existentialtype.wordpress.com/2011/03/17/parallelism-is-not-concurrency/
[rp]:http://concur.rspace.googlecode.com/hg/talk/concur.html#slide-54
