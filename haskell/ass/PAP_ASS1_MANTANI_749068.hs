data Elem = Dot | Star

data BSTree a = Nil | Node a (BSTree a) (BSTree a)

-- [countStar] Count number of Star in [Elem] {{{
countStar :: [Elem] -> Int
countStar [] = 0
countStar (Star:xs) = 1 + countStar xs
countStar (_:xs) = countStar xs
-- }}}
-- [printableSeq] (Dot '.', Star '*') {{{
printableSeq :: [Elem] -> String
printableSeq [] = ""
printableSeq (Star:xs) = '*' : printableSeq xs
printableSeq (_:xs) = '.' : printableSeq xs
-- }}}
-- [deserializeSeq] From String to Elem (Dot '.', Star '*') {{{
deserializeSeq :: String -> [Elem]
deserializeSeq "" = []
deserializeSeq ('*':xs) = Star : deserializeSeq xs
deserializeSeq (_:xs) = Dot : deserializeSeq xs
-- }}}
-- [swapSeq] replace every Star with Dot and viceversa {{{
swapSeq :: [Elem] -> [Elem]
swapSeq [] = []
swapSeq (Star:xs) = Dot : swapSeq xs
swapSeq (_:xs) = Star : swapSeq xs
-- }}}
-- [zipSeq] replace Dot+ with a single one {{{
zipSeq :: [Elem] -> [Elem]
zipSeq [] = []
zipSeq (Star:xs) = Star : zipSeq xs
-- haskell lazy? me too, I use _ instead of Dot, I can Elem has only Star & Dot
zipSeq (_:xs) = Dot:zipSeq(zipSeq' xs)
  where zipSeq' :: [Elem] -> [Elem] -- remove all subsequent dots
        zipSeq' [] = []
        zipSeq' (Dot:xs') = zipSeq' xs'
        zipSeq' s = s
-- }}}
-- [maxStarSeq] self explained {{{
maxStarSeq :: [Elem] -> Int
maxStarSeq list = maxStarSeq' list 0 0
  where maxStarSeq' :: [Elem] -> Int -> Int -> Int
        maxStarSeq' [] x y -- (x) current stars cluster (y) max cluster
          | x > y = x
          | otherwise = y
        maxStarSeq' (Star:xs) x y = maxStarSeq' xs (x+1) y
        maxStarSeq' (Dot:xs) x y -- time to reset x
          | x > y = maxStarSeq' xs 0 x -- is current > old
          | otherwise = maxStarSeq' xs 0 y
-- }}}
-- [matchSeq] same sequences of Star, Dot?! Which ones?{{{
matchSeq :: [Elem] -> [Elem] -> Bool
matchSeq [] [] = True
matchSeq (Star:_) [] = False
matchSeq [] (Star:_) = False
matchSeq (Dot:xs) (Dot:ys) = matchSeq xs ys -- discard all dots
matchSeq (Dot:xs) y = matchSeq xs y
matchSeq x (Dot:ys) = matchSeq x ys
matchSeq (_:xs) (_:ys) = matchSeq xs ys -- (both Star) check only stars clusters
-- }}}
-- [occ] {{{ determina una lista delle tuple a 2 elementi, in cui il primo indica la
-- lunghezza di una sequenza di Star presente nella lista e il secondo la lista
-- delle posizioni in cui tale sequenza compare nella lista (la prima posizione
-- è la numero 1).
occ :: [Elem] -> [(Int, [Int])]
occ list = occ' list [] 1 0
  where
    memo :: [(Int, [Int])] -> Int -> Int -> [(Int, [Int])]
    memo [] i n = [(n,[i-n])] -- new cluster
    memo (t@(s,l):xs) i n
      | s == n = [(s,l ++ [i-n])] ++ xs -- there is already a cluster of len s
      | otherwise = [t] ++ memo xs i n
    occ' :: [Elem] -> [(Int, [Int])] -> Int -> Int -> [(Int, [Int])]
    occ' [] tuples index nstar
      | nstar > 0 = memo tuples index nstar
      | otherwise = tuples
    occ' (Dot:xs) tuples index nstar
      | nstar > 0 = occ' xs (memo tuples index nstar) (index+1) 0
      | otherwise = occ' xs tuples (index+1) 0
    occ' (Star:xs) tuples index nstar = occ' xs tuples (index+1) (nstar+1)

-- }}}
-- [countStarInTree] self explained {{{
countStarInTree :: BSTree Elem -> Int
countStarInTree Nil = 0
countStarInTree (Node Star l r) = 1 + countStarInTree l + countStarInTree r
countStarInTree (Node _ l r) = countStarInTree l + countStarInTree r
-- }}}
-- [pathTree] max branch's length of Star {{{
pathTree :: BSTree Elem -> Int
pathTree Nil = 0
pathTree (Node Dot _ _) = 0
pathTree (Node Star l r)
  | x > y = x
  | otherwise = y
    where
      x = 1 + pathTree l
      y = 1 + pathTree r
-- }}}

{- Testing: {{{
-- Not only lazy in haskell, I want write a BSTree more easily :D
-- Not really useful but I speed up tests
speedUp :: String -> BSTree Elem
speedUp ('d':xs) = Node Dot (speedUp xs) (speedUp xs)
speedUp ('s':xs) = Node Star (speedUp xs) (speedUp xs)
speedUp _ = Nil
}}}-}
