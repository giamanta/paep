{- -----------------------------------------------------------------------------
 - License : [GPLv2]                                                       {{{ -
 - Author  : Giacomo Mantani                                                   -
 - ID      : 749068                                                            -
 - -----------------------------------------------------------------------------
 -                                                                             -
 - Description:                                                                -
 - ------------                                                                -
 - Il seguente modulo e' stato realizzato come soluzione all' Assignment #3    -
 - del corso Programmazione Avanzata e Paradigmi.                              -
 -                                                                             -
 - I punti chiave dell'esercizio sono:                                         -
 -                                                                             -
 - [1] data una lista di StarSeq computa lunghezza e posizione della sequenza  -
 - più lunga                                                                   -
 -                                                                             -
 - [2] data una lista di StarSeq, stampa in uscita le sequenze in ordine       -
 - crescente di lunghezza, rappresentandole come sequenze di *, una sequenza   -
 - per ogni linea                                                              -
 -                                                                             -
 - [!] Testing, implementativamente libero ho scelto di fare una funzione      -
 -      (testThemAll) che in modo interattivo portasse l'utente attraverso     -
 -      ogni punto importante dell'assignment                                  -
 -                                                                         }}} -
 - -------------------------------------------------------------------------- -}
module PAP_ASS3_MANTANI_749068 (getMaxSeq, printSeqs, testThemAll) where
import Data.List

data StarSeq = Star StarSeq | End
  deriving (Show)

len :: StarSeq -> Int
len End = 0
len (Star tl) = 1 + len tl

getMaxSeq :: [StarSeq] -> (Int,Int) -- [1]
getMaxSeq l = foldr
  (\ (l0, prev) (l1, _) -> if (l0 > l1) then (l0, prev) else (l1, prev+1))
  (len End, 0)
  (map (\ s -> (len s, 1)) l)

printSeqs :: [StarSeq] -> IO () -- [2]
printSeqs l = printSeqs' $ sort $ map len l
  where
    printSeqs' :: [Int] -> IO()
    printSeqs' (x:xs) = printSeq x >> printSeqs' xs
      where printSeq :: Int -> IO()
            printSeq n
              | n <= 0 = putStrLn ""
              | otherwise = putChar '*' >> printSeq (n-1)
    printSeqs' _ = return ()

testThemAll :: IO() -- [!] {{{
testThemAll =
  let
    s0 = []
    s1 = [(Star (Star End))]
    s2 = [(Star (Star (Star (Star End)))), (Star (Star End))]
    s3 = [(Star (Star (Star End))), (Star (Star (Star (Star End)))), (Star End), (Star (Star End))]
    s4 = [End, (Star (Star (Star End))), (Star (Star (Star (Star End)))), (Star End), (Star (Star End))]
    s5 = [End, (Star (Star (Star End))), (Star (Star (Star (Star End)))), End, (Star End), (Star (Star End))]
    s = [s0,s1,s2,s3,s4,s5]
  in
    printAll s
      where
        printAll :: [[StarSeq]] -> IO()
        printAll [] = return ()
        printAll (x:xs) = do
          putStrLn "[StarSeq]"
          print x
          putStrLn ""
          putStrLn "[getMaxSeq]"
          print $ getMaxSeq x
          putStrLn ""
          putStrLn "[printSeqs]"
          printSeqs x
          putStrLn "---------------"
          printAll xs -- }}}
