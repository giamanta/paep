{- -----------------------------------------------------------------------------
 - License : [GPLv2]                                                       {{{ -
 - Author  : Giacomo Mantani                                                   -
 - ID      : 749068                                                            -
 - -----------------------------------------------------------------------------
 -                                                                             -
 - Description:                                                                -
 - ------------                                                                -
 - Dato un albero binario t di interi e un valore intero d, stampa gli         -
 - elementi dell'albero la cui distanza dal nodo radice è pari a d.            -
 -                                                                         }}} -
 - -------------------------------------------------------------------------- -}
module PAP_ASS5_MANTANI_749068 (print_nodes_at_dist, testThemAll) where
data BSTree a = Nil | Node a (BSTree a)(BSTree a)

print_nodes_at_dist::BSTree Int -> Int -> IO()
print_nodes_at_dist b d = print_nodes_at_dist' b d 0
  where
    print_nodes_at_dist'::BSTree Int -> Int -> Int -> IO()
    print_nodes_at_dist' Nil _ _ = return()
    print_nodes_at_dist' (Node v l r) dip c
      | c == dip = print v
      | otherwise = print_nodes_at_dist' l dip (c+1) >>
                    print_nodes_at_dist' r dip (c+1)

testThemAll :: IO()
testThemAll =
  let
    t = (Node 6
        ( Node 1 ( Node 4
                   ( Node 2 Nil Nil)
                   Nil)
          Nil)
        ( Node 3 ( Node 9
                   ( Node 3 Nil Nil)
                   Nil)
          (Node 7 Nil Nil)))
  in
    putStrLn "[Test 1] should print 6" >>
    print_nodes_at_dist t 0 >>
    putStrLn "[Test 2] should print 4 9 7" >>
    print_nodes_at_dist t 2 >>
    putStrLn "[Test 3] should print nothing" >>
    print_nodes_at_dist t 6
