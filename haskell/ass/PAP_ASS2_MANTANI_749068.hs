{- -----------------------------------------------------------------------------
 - License : [GPLv2]
 - Author  : Giacomo Mantani
 - ID      : 749068
 - -----------------------------------------------------------------------------
 -
 - Description:
 - ------------
 - Il seguente modulo e' stato realizzato come soluzione all' Assignment #2
 - del corso Programmazione Avanzata e Paradigmi.
 -
 - I punti chiave dell'esercizio sono:
 -
 - [ 1] istanza della classe CShape con tipo Shape, definendo le funzioni
 -     (perim,move) per ogni Shape
 -
 - [2a] funzione moveShapes
 - [2b] funzione inBBox
 - [2c] funzione maxArea
 - [2d] funzione makeShapeTree
 -
 - [3a] classe Drawable che estende CShape
 - [3b] istanza Drawable Shape che implementa una funzione draw per disegnare
 -      la Shape nel terminale (ascii art is fun! CLI <3)
 - [3c] implementare la funzione drawAll
 -
 - [!!] Testing, implementativamente libero ho scelto di fare una funzione
 -      (testThemAll) che in modo interattivo portasse l'utente attraverso
 -      ogni punto importante dell'assignment
 -
 - -----------------------------------------------------------------------------
 -}

module PAP_ASS2_MANTANI_749068 (P2d, V2d, Shape (..), BSTree(..),
                                CShape, Drawable, draw, moveShapes,
                                inBBox, maxArea, makeShapeTree,
                                drawAll, testThemAll) where

import Screen
import GHC.Float

type P2d = (Int, Int)
type V2d = (Int, Int)

data Shape =  Line (P2d, P2d)         | -- {{{
              Triangle (P2d, P2d, P2d)|
              Rectangle (P2d, P2d)    |
              Circle (P2d, Float)     |
              Composition [Shape]     |
              Nilshape
                deriving (Show,Eq) -- }}}

data BSTree a = Nilbs | Node a (BSTree a) (BSTree a)
  deriving Show

-- [Classes and instances ] ---------------------------------------------------
--
class CShape c where
  perim :: c -> Float
  move :: c -> V2d -> c

class (CShape a) => Drawable a where -- [3a]
  draw :: a -> IO()

instance CShape Shape where -- [ 1] {{{
  perim Nilshape = 0
  perim (Line (p1, p2)) = distance p1 p2

  perim (Triangle (p1, p2, p3)) =
    distance p1 p2 + distance p2 p3 + distance p1 p3

  perim (Rectangle (p0@(x0, y0), (x1, y1))) =
    2 * (distance p0 (x0, y1) + distance p0 (x1, y0))

  perim (Circle (_, r)) = circumference r

  perim (Composition []) = 0
  perim (Composition (c:cs)) = perim c + perim (Composition cs)

  move Nilshape _ = Nilshape
  move  (Line (p1, p2)) v =
    Line (movePV p1 v, movePV p2 v)

  move  (Triangle (p1, p2, p3)) v =
    Triangle (movePV p1 v, movePV p2 v, movePV p3 v)

  move  (Rectangle (p1, p2)) v = Rectangle (movePV p1 v, movePV p2 v)

  move  (Circle (c, r)) v = Circle (movePV c v, r)

  move  (Composition []) _ = Composition []
  move  (Composition (c:cs)) v =
    Composition(move c v : [move (Composition cs) v]) -- }}}

instance Ord Shape where
  s1 `compare` s2 = minX s1 `compare` minX s2

instance Drawable Shape where -- [3b] {{{
  draw (Line (pa, pb)) = plotLine pa pb
  draw (Triangle (pa, pb, pc)) = plotLine pa pb >>
                                 plotLine pa pc >>
                                 plotLine pb pc
  draw (Rectangle (pa@(xa, ya), pb@(xb, yb))) =
    plotLine pa (xa, yb) >> plotLine pa (xb, ya) >>
    plotLine pb (xa, yb) >> plotLine pb (xb, ya)
  draw (Circle (c, r)) = plotCirle c $ float2Int r
  draw (Composition (x:xs)) = draw x >> draw (Composition xs)
  draw _ = return ()
-- }}}

-- [my functions] --------------------------------------------------------------
--
-- Utils {{{
distance :: P2d -> P2d -> Float
distance (x1,y1) (x2,y2) = sqrt $ int2Float(x2-x1)**2 + int2Float(y2-y1)**2

movePV :: P2d -> V2d -> P2d
movePV (x, y) (dx, dy) = (x+dx, y+dy)

circumference :: Float -> Float
circumference r = 2 * pi * r

isPointInBox :: P2d -> (P2d, P2d) -> Bool -- {{{
isPointInBox (w, h) ((topLw, topLh), (bottomRw, bottomRh))
  | topLw > w || topLh > h = False
  | bottomRw < w || bottomRh < h = False
  | otherwise = True -- }}}

area :: Shape -> Float -- {{{
area (Triangle ((x1,y1),(x2,y2),(x3,y3))) =   -- shoelace formula
  0.5 * int2Float (abs $ (x1-x3)*(y2-y1)-(x1-x2)*(y3-y1))
area (Circle (_, r)) = pi * r * r
area (Rectangle (p0@(x0,y0),(x1,y1))) = distance p0 (x0, y1) * distance p0 (x1, y0)
area (Composition (c:cs)) = area c + area (Composition cs)
area _ = 0 -- Nilshape, Line and Composition [] have not area }}}

minX :: Shape -> Int -- {{{
minX (Line ((x0,_),(x1,_))) = min x0 x1
minX (Triangle ((x0,_),(x1,_),(x2,_))) = min x0 x1 `min` x2
minX (Rectangle ((x0,_),(x1,_))) = min x0 x1
minX (Circle ((x0,_),r)) = x0 - float2Int r
minX (Composition (x:xs)) = min (minX x) (minX (Composition xs))
minX _ = 0 -- Nilshape and (Composition []) }}}

ordByX :: [Shape] -> [Shape] -- {{{
ordByX [] = []
ordByX (x:xs) =
  ordByX [y | y <- xs, y < x] ++
  [x] ++
  ordByX [y | y <- xs, y >= x] -- }}}

plotLine :: P2d -> P2d -> IO() -- Bresenhams line algorithm {{{
plotLine p0@(x0,y0) p1@(x1,y1)
  | p0 == p1 = plotDot p0
  | otherwise = plotDot p0 >> plotLine' p0 p1 (2*(dy+dx)) >> plotDot p1
  where
    dx = abs $ x1-x0
    sx = if x0 < x1 then 1 else -1
    dy = - (abs $ y1-y0)
    sy = if y0 < y1 then 1 else -1
    plotLine' :: P2d -> P2d -> Int -> IO()
    plotLine' pa@(xa,ya) pb@(xb,yb) err
      | err > dy && xa == xb = return ()
      | err < dx && ya == yb = return ()
      | otherwise = plotDot pa >> plotLine' (xn,yn) pb (2*errn)
        where
          xn = if err > dy then xa+sx else xa
          yn = if err < dx then ya+sy else ya
          errn = (if err > dy then dy else 0) +
                 (if err < dx then dx else 0) + err -- }}}

plotDot :: P2d -> IO()
plotDot p = writeAt p "○"

plotCirle :: P2d -> Int -> IO() -- {{{
plotCirle cp radius = plotCirle' cp radius (1-radius) 0
  where
    plotCirle' :: P2d -> Int -> Int -> Int -> IO()
    plotCirle' p@(xa,ya) r e y
      | r >= y = do
        plotDot ( r+xa, y+ya)
        plotDot ( y+xa, r+ya)
        plotDot (-r+xa, y+ya)
        plotDot (-y+xa, r+ya)
        plotDot (-r+xa,-y+ya)
        plotDot (-y+xa,-r+ya)
        plotDot ( r+xa,-y+ya)
        plotDot ( y+xa,-r+ya)
        if e <  0 then plotCirle' p r (e+2*y+1) (y+1)
        else plotCirle' p (r-1) (e+2*(y-r)+1) (y+1)
      | otherwise = return () -- }}}

nextTest :: String -> IO() -- interactive shell {{{
nextTest s = do
  goto(0,3)
  putStrLn $ " -> Next Test: [" ++ s ++ "] (press a char to continue)"
  _ <- getChar
  cls
  return () -- }}}

-- }}}

-- [2] -------------------------------------------------------------------------
--
moveShapes :: [Shape] -> V2d -> [Shape] -- [2a] {{{
moveShapes [] _ = []
moveShapes (x:xs) v = move x v : moveShapes xs v -- }}}

inBBox :: [Shape] -> (P2d, P2d) -> [Shape] -- [2b] {{{
inBBox [] _ = []
inBBox (x:xs) b = inBBox' x b ++ inBBox xs b
  where
    inBBox' :: Shape -> (P2d, P2d) -> [Shape]
    inBBox' Nilshape _ = []
    inBBox' t@(Triangle (p1,p2,p3)) box
      | isPointInBox p1 box && isPointInBox p2 box && isPointInBox p3 box = [t]
      | otherwise = []
    inBBox' c@(Circle ((x0, y0), r)) box
      | isPointInBox (x0 + float2Int r,y0) box && isPointInBox (x0 - float2Int r,y0) box &&
        isPointInBox (x0,y0 + float2Int r) box && isPointInBox (x0,y0 - float2Int r) box = [c]
      | otherwise = []
    inBBox' l@(Line (p1,p2)) box -- Line and Rectangle have the same check
      | isPointInBox p1 box && isPointInBox p2 box = [l]
      | otherwise = []
    inBBox' l@(Rectangle (p1,p2)) box -- Line and Rectangle have the same check
      | isPointInBox p1 box && isPointInBox p2 box = [l]
      | otherwise = []
    inBBox' (Composition []) _ = []
    inBBox' (Composition (y:ys)) box = inBBox' y box ++ inBBox ys box -- }}}

maxArea :: [Shape] -> Shape -- [2c] {{{
maxArea [] = Nilshape
maxArea l = maxArea' l Nilshape 0
  where maxArea' :: [Shape] -> Shape -> Float -> Shape
        maxArea' [] smax _ = smax
        maxArea' (x:xs) smax m
          | m < ax = maxArea' xs x ax
          | otherwise = maxArea' xs smax m
          where
            ax = area x -- }}}

makeShapeTree :: [Shape] -> BSTree Shape -- [2d] {{{
makeShapeTree l = makeShapeTree' $ ordByX l
  where
    makeShapeTree' [] = Nilbs
    makeShapeTree' (x:xs) = Node x Nilbs (makeShapeTree' xs) -- }}}

drawAll :: [Shape] -> IO() -- [3c]
drawAll = foldr ((>>) . draw) (return ()) --

-- [Testing things] ------------------------------------------------------------
--
testThemAll :: IO() -- [!!] {{{
testThemAll =
  let
  glider = [Line ((1,3),(3,3)),
            Line ((3,2),(3,2)),
            Line ((2,1),(2,1))]
  ls = [Line ((5,5),(9,9)),
        Line ((5,9),(7,7)),
        Triangle ((11,5),(11,9),(15,9)),
        move (Triangle ((12,5),(5,12),(19,12))) (40,0),
        Rectangle ((5,11),(15,14)),
        Circle ((24,24), 7),
        move (Composition glider) (28,10)
        ]
  in do
    cls
    goto(0,0)
    putStrLn "[Info] Starting interactive testing mode.."
    putStrLn "[Info] First of all a drawAll.."
    drawAll ls
    nextTest "perim prev Shapes"
    print $ map perim ls
    nextTest "moveShapes (implicit move)"
    drawAll $ moveShapes ls (20,0)
    nextTest "inBBox"
    drawAll $ inBBox ls ((20,8),(35,35))
    nextTest "maxArea"
    draw $ maxArea ls
    nextTest "makeShapeTree"
    print $ makeShapeTree ls
    return () -- }}}
