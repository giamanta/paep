-------------------------------------------------------------------------------
-- ( First exercises )
-- Hints: Base cases
--       + more deep cases

-- {{{ len
len :: [a] -> Int
len [] = 0
-- len(x:xs) = 1 + len xs
-- e' piu' elegante! il valore prescinde dalla scelta e x non viene usato
-- all'interno quindi possiamo ometterlo
len(_:xs) = 1 + len xs
-- }}}
-- {{{ append
append :: [a] -> [a] -> [a]
append [] xs = xs
-- Generalmente e' il primo che conta per caso unita'
-- una lista con almeno un elemento (x) concat con ys, e' x concat l'append
-- della coda della prima lista con ys
append (x:xs) ys = x : append xs ys
-- }}}
-- {{{ 1) isPresent
{-
1) Implementare la funzione isPresent che data una lista di interi e un elemento
intero, restituisce vero o falso a seconda che l’elemento sia presente o meno
nella lista

isPresent 1 [] -> False
isPresent 3 [1,4,2,5] -> False
isPresent 3 [1,4,3,5] -> True

-}

isPresent :: (Eq) a => a -> [a] -> Bool
-- Dove 'a' risulta essere una classe
isPresent _ [] = False
isPresent y (x:xs)
  | x == y = True
  | otherwise = isPresent y xs

-- isPresent x (x:_) = True potevi farlo in prolog ma in haskell i pattern non
-- possono condividere i simboli (XD)

--isPresent v (x:xs) = if v == x then True else isPresent v xs
-- ricorda che if e' come una funzione higher order, non e' elegante
-- }}}
-- {{{ 2) rev
{-
2) Implementare la funzione rev che data una lista di elementi generica,
restituisce la lista con gli elementi in ordine inverso

rev [1,4,6] -> [6, 4, 1]
rev [“ab”,”bb”,”bc”] -> [“bc”,”bb”,”ab”]
-}

rev :: [a] -> [a]
rev [] = []
rev (x:xs) = rev xs ++ [x]
-- }}}
-- {{{ 3) removeAll
{-
3) Implementare la funzione removeAll che data una lista di elementi generica e
un elemento, restituisca una lista pari a quella passata senza tutti gli
elementi pari all’elemento passato

removeAll [1,4,6,4] 8 -> [1, 4, 6]
removeAll [1,4,6,4] 4 -> [1, 6]
-}

removeAll :: Eq(a) => [a] -> a -> [a]
removeAll [] _ = []
-- removeAll [x] _
--   | v == x = []
--   | otherwise = [x]
removeAll (x:xs) y
  | x == y = res
  | x /= y = x : res
  where res = removeAll xs y
-- }}}
-- {{{ 4) merge
{- 4) Implementare la funzione merge che date due liste ordinate di elementi
interi, computa una sola lista ordinata
esempio:
merge [1,6,8] [5,9,20,21] → [1,5,6,8,9,20,21] -}

merge :: (Ord a) => [a] -> [a] -> [a]
-- merge [] [] = [] *NB* non serve, con vuota e vuota, viene soddisfatta da 1+2
merge xs [] = xs
merge [] xs = xs
merge (x:xs) (y:ys)
  | x > y = y : merge (x:xs) ys
  | otherwise = x : merge xs (y:ys)

-- }}}
-- {{{ 5) isPresentInBST testTree
{- -
 - Data la struttura dati:
 - data BSTree a = Nil | Node a (BSTree a) (BSTree a)
 -
 - che rappresenta un albero binario di ricerca, implementare la funzione
 - isPresentInBST generica che dato un valore e un BSTree restituisce vero o
 - falso a seconda che il valore sia o meno presente
 -
 - isPresentInBST :: a -> BSTree a -> Bool
 -
 - Supponendo di avere
 -
 - testTree :: BSTree String
 -
 - testTree = (“faro”, (“caco”, (“albero”,Nil,Nil), (“dado”,Nil, Nil)), (“luce”,
 - (“iodio”,Nil,Nil), Nil))
 -
 - allora
 -
 - isPresentInBST testTree “albero” →  True
 -
 - isPresentInBST testTree “suola” →  False
 - -}

data BSTree a = Nil | Node a (BSTree a) (BSTree a)
-- Ord a, e' la classe a cui fanno riferimento i controlli dentro
isPresentInBST :: (Ord a) => a -> BSTree a -> Bool
isPresentInBST _ Nil = False -- pattern matching
-- Che pattern posso specificare? potrei mettere una variabile, intendendo che
-- e' associata al nodo, oppure piu' elegantemente:
-- Nodo y + albero sx + albero dx, che e' il pattern per un albero!
isPresentInBST x (Node y l r)
 | x == y = True
 | x < y = isPresentInBST x l
 | otherwise = isPresentInBST x r

testTree :: BSTree String
testTree = Node "lored" (Node "ipsum" Nil Nil) Nil
-- }}}
{- {{{ 6)
Implementare la funzione buildBST che, data la una lista di stringhe, costruisce
l’albero binario ordinato che contiene le stringhe e la loro posizione
all’interno della lista.  }}}-}
{- {{{ 7)
Sia Shape un tipo di dato algebrico che rappresenta una figura bidimensionale
(Triangolo, Quadrato, Cerchio), ognuna identificata da un identificatore
stringa. Implementare la funzione calcPerim :: [Shape] -> [(String,Int)] che
data una lista di Shape computi la lista di tuple date da identificatore della
Shape e rispettivo perimetro.  }}}-}
{- {{{ 8)
Scrivere una funzione encode :: Int -> String -> String che implementi il
cifrario di Cesare. Tale codifica consiste nel rimpiazzare ogni lettera della
stringa passata come argomento con la lettera che sta n posti più avanti
nell’alfabeto, dove n è il primo parametro passato, facendo il wrap se si
deborda oltre la fine dell’alfabeto.  }}}-}
{- {{{ 8)
Scrivere una funzione freqs :: String -> [Float] che data una stringa computi la
tabella delle frequenze (in percentuale) relative ad ogni lettera dell’alfabeto,
ovvero ogni lettera dell’alfabeto quante volte compare nella stringa (in
percentuale).  }}}-}
{- {{{ 9)
Scrivere una funzione crack :: String -> String che tenta di decifrare una
stringa codificata con il cifrario di Cesare, considerando la lista delle
frequenze.  }}}-}
